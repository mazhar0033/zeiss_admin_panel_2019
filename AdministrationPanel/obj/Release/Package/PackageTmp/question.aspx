﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" CodeBehind="question.aspx.cs" Inherits="AdministrationPanel.question" %>

<asp:Content ContentPlaceHolderID="Body" runat="server" ID="Body">
    <section class="content-header">
        <h1>Visitor Questioner
                       
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="default.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Questioner</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-6 connectedSortable ui-sortable">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-clipboard"></i>
                        <h3 class="box-title">Share my email with other DPMC attendees</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvQuestion1" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                    <Columns>
                                        <asp:HyperLinkField HeaderText="Share my email with other DPMC attendees" DataTextField="String" DataNavigateUrlFields="String" DataNavigateUrlFormatString="registrations.aspx?ShareMyEmail={0}" />
                                        <asp:BoundField DataField="Count" HeaderText="Count" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--<div class="col-lg-6 connectedSortable ui-sortable">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-clipboard"></i>
                        <h3 class="box-title">Share my email with other DPMC attendees</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvQuestion2" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                    <Columns>
                                        <asp:HyperLinkField HeaderText="Share my email with other DPMC attendees" DataTextField="String" DataNavigateUrlFields="String" DataNavigateUrlFormatString="registrations.aspx?ShareMyEmail={0}" />
                                        <asp:BoundField DataField="Count" HeaderText="Count" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <%--  <div class="col-lg-6 connectedSortable ui-sortable">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-clipboard"></i>
                        <h3 class="box-title">Are you a member? </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvQuestion3" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                    <Columns>
                                        <asp:HyperLinkField HeaderText="Are you a member? " DataTextField="String" DataNavigateUrlFields="String" DataNavigateUrlFormatString="registrations.aspx?Areyouamember={0}" />
                                        <asp:BoundField DataField="Count" HeaderText="Count" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
            <div class="col-lg-6 connectedSortable ui-sortable">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-clipboard"></i>
                        <h3 class="box-title">Accompanying Person</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvQuestion4" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                    <Columns>
                                        <asp:HyperLinkField HeaderText="Accompanying Person:" DataTextField="String" DataNavigateUrlFields="String" DataNavigateUrlFormatString="registrations.aspx?AccompanyingPerson={0}" />
                                        <asp:BoundField DataField="Count" HeaderText="Count" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 connectedSortable ui-sortable">
                <div class="box box-success">
                    <div class="box-header">
                        <i class="fa fa-clipboard"></i>
                        <h3 class="box-title">Ticket type</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-sm-12">
                                <asp:GridView ID="gvQuestion5" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                    <Columns>
                                        <asp:HyperLinkField HeaderText="Ticket type:" DataTextField="String" DataNavigateUrlFields="String" DataNavigateUrlFormatString="registrations.aspx?RegisterType={0}" />
                                        <asp:BoundField DataField="Count" HeaderText="Count" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</asp:Content>
