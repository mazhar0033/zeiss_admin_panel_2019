﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" CodeBehind="users.aspx.cs" Inherits="AdministrationPanel.Users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server" ID="Body">
    <!-- User Create MODAL -->
    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-plus"></i>Add User </h4>
                </div>
                <form action="#" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Full Name:</span>
                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control" />                               
                            </div>
                             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="txtName"
                                    ErrorMessage="Full is a required field."
                                    ForeColor="Red">
                                </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Username:</span>
                                <asp:TextBox ID="txtUsername" runat="server" CssClass="form-control" />
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                    ControlToValidate="txtUsername"
                                    ErrorMessage="Username is a required field."
                                    ForeColor="Red">
                                </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Password:</span>
                                <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" />
                            </div>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                    ControlToValidate="txtPassword"
                                    ErrorMessage="Password is a required field."
                                    ForeColor="Red">
                                </asp:RequiredFieldValidator>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Access Level:</span>
                                <asp:DropDownList ID="ddlAccess" CssClass="form-control" runat="server">
                                    <asp:ListItem Value="SuperAdmin">Super Admin</asp:ListItem>
                                    <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer clearfix">

                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Discard</button>

                        <asp:LinkButton ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="btn btn-primary pull-left"><i class="fa fa-plus"></i> Create</asp:LinkButton>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <section class="content-header">
        <h1>Users
                       
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>User</a></li>
            <li class="active">Users</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="pull-right box-tools">
                            <a class="btn btn-app" data-target="#compose-modal" data-toggle="modal">
                                <i class="fa fa-plus"></i>
                                Add</a>
                        </div>
                        <i class="fa fa-user"></i>
                        <h3 class="box-title">Users</h3>
                    </div>
                    <div class="box-body">
                        <div id="gridReg" runat="server" class="box-body table-responsive">
                            <div id="registration" class="dataTables_wrapper form-inline" role="grid">
                                <asp:GridView runat="server" CssClass="table table-bordered table-striped dataTable" DataKeyNames="ID" AutoGenerateColumns="false" ID="gvUsers">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="Name" />
                                        <asp:BoundField DataField="Username" HeaderText="Username" />
                                        <asp:BoundField DataField="Type" HeaderText="Access Level" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
