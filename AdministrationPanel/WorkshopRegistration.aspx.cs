﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class WorkshopRegistration : System.Web.UI.Page
    {
        protected MemberInfo member = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string UserType = this.member.Type;
            if (UserType != "SuperAdmin")
            {
                btnGenerateCSV.Visible = false;
            }
            this.Page.Title = "Admin - Registrations";
            PopulateGV();
        }

        protected void btnGenerateCSV_Click(object sender, EventArgs e)
        {
         

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("EvePrc_Get_Admin_WorkShopDetails", conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        da = new SqlDataAdapter(cmd);
                        da.Fill(dSet, "Table");
                        CsvExport csv = new CsvExport();
                        foreach (DataRow row in dSet.Tables[0].Rows)
                        {
                            csv.AddRow();
                            csv["Sln No"] = row["WID"];
                            csv["WorkShop Name"] = row["WorkShopName"];
                            csv["WorkShop Code"] = row["WorkShopCode"];
                            csv["Total Capacity"] = row["MaxCount"];
                            csv["Registered Seats"] = row["RegCount"];
                            csv["Remaining Seats"] = row["Remaing"];
                        }

                        string sCsv = csv.Export();
                        Response.ContentType = "text/csv";
                        Response.AddHeader("Content-Disposition", "attachment; filename=WorkshopRegistration.csv");
                        Response.Write(sCsv);
                        Response.End();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }

            }
        }

        private void PopulateGV()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"exec EvePrc_Get_Admin_WorkShopDetails";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        int count = dSet.Tables[0].Rows.Count;
                        ViewState["Count"] = count;
                        totalCount.Text = "Total - " + count.ToString();
                        ViewState["Table"] = dSet;
                        gvRegistrations.DataSource = dSet.Tables["Table"].DefaultView;
                        gvRegistrations.DataBind();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }


        protected void gvRegistrations_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRegistrations.PageIndex = e.NewPageIndex;
            DataSet ds = (DataSet)ViewState["Table"];
            gvRegistrations.DataSource = ds;
            gvRegistrations.DataBind();
           
        }
    }
}