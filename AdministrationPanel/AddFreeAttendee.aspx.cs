﻿using OnlineRegistration.Logger;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class AddFreeAttendee : System.Web.UI.Page
    {
        protected MemberInfo member = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string UserType = this.member.Type;
            if (UserType != "SuperAdmin")
            {
                btnRegister.Visible = false;
            }
            this.Page.Title = "Admin - Registrations";
            if (!IsPostBack)
            {
                PopulateCountry();
                UpdateStatus();
            }
        }

        public void PopulateCountry()
        {
            DataTable country = new DataTable();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT ID, CountryCode,Country,Code FROM Country", con);
                    adapter.Fill(country);

                    ddlNationality.DataSource = country;
                    ddlNationality.DataTextField = "Country";
                    ddlNationality.DataValueField = "ID";
                    ddlNationality.DataBind();

                    ddlCountry.DataSource = country;
                    ddlCountry.DataTextField = "Country";
                    ddlCountry.DataValueField = "ID";
                    ddlCountry.DataBind();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            ddlNationality.Items.Insert(0, new ListItem(string.Empty, ""));
            ddlCountry.Items.Insert(0, new ListItem(string.Empty, ""));
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            string sFirstName = txtFirstName.Text;
            string sLastName = txtLastName.Text;
            string sDesignation = txtDesignation.Text;
            string sCompany = txtCompany.Text;
            string sCountry = ddlCountry.SelectedItem.Text;
            string sEmail = txtEmail.Text;
            string sNationality = ddlNationality.SelectedItem.Text;
            string sTicketType = rdotickettype.SelectedValue;
            string sSector = rdoSector.SelectedValue;
            string sDOB = GetRandomdate("25-34");
            string sLanguage = "English";
            string sCity = txtCity.Text;
            string sComments = txtComments.Text;
            

            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.Parameters.Clear();


                        #region EmailCheck
                        cmd.CommandText = "SELECT COUNT(*) FROM Registrations WHERE Email=@Email and Status in (-2,-1,2) and convert(varchar(10),DateRegister,121) =   convert(varchar(10),getdate(),121) and  DATEDIFF(MI,DateRegister, getdate()) <=5";
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));

                        bool isExisit = ((int)cmd.ExecuteScalar() > 0);
                        cmd.Parameters.Clear();

                        if (isExisit)
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = "Your previous transaction is still not completed. Please try after some time.";
                            return;
                        }
                        #endregion 

                       
                        //Insert Detials
                        cmd.Parameters.Clear();
                        cmd.CommandText = @"Insert into Registrations(FirstName, LastName, Designation,Company,Nationality,CountryOfResidence, CityOfResidence,Email,VisitorOrExhibitor,PublicOrPrivate, DateRegister,Status,Comments,Language)
                                        Values(@FirstName, @LastName,@Designation,@Company,@Nationality,@CountryOfResidence, @City, @Email,@RegistrationType, @PublicOrPrivate,@DateRegister,@Status,@Comments,@Language)" + "Select Scope_Identity()";

                        cmd.Parameters.Add(new SqlParameter("@FirstName", sFirstName));
                        cmd.Parameters.Add(new SqlParameter("@LastName", sLastName));
                        cmd.Parameters.Add(new SqlParameter("@Designation", sDesignation));
                        cmd.Parameters.Add(new SqlParameter("@Company", sCompany));
                        cmd.Parameters.Add(new SqlParameter("@Nationality", sNationality));
                        cmd.Parameters.Add(new SqlParameter("@CountryOfResidence", sCountry));
                        cmd.Parameters.Add(new SqlParameter("@City", sCity));
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                        cmd.Parameters.Add(new SqlParameter("@RegistrationType", sTicketType));
                        cmd.Parameters.Add(new SqlParameter("@PublicOrPrivate", sSector));
                        cmd.Parameters.Add(new SqlParameter("@DateRegister", DateTime.Now));
                        cmd.Parameters.Add(new SqlParameter("@Status", "2"));
                        cmd.Parameters.Add(new SqlParameter("@Comments", sComments));
                        cmd.Parameters.Add(new SqlParameter("@Language", "Arabic"));
                        int status = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        cmd.Parameters.Clear();

                       

                        if (status > 0)
                        {
                            //SendConfirmation(status.ToString());
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Data has been submitted successfully'); ", true);
                            Response.Redirect("registrations.aspx");
                        }
                        else
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = "status is null.";
                            return;
                        }
                    }


                }
                catch (Exception ex)
                {
                    litMessage.Text = ex.ToString();
                    ErrorLogger.Log("AddFreeAttendee.cs", "btnRegister_Click", ex, "HttpPost"); 
                }
            }
        }

        private void UpdateStatus()
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.CommandText = "Update Registrations set status = 0 where status='-2'  and  DATEDIFF(MI,DateRegister, getdate()) >=30";
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("AddFreeAttendee.cs", "UpdateStatus", ex, "HttpPost");
                    divError.Attributes.Add("style", "display:block");
                    litMessage.Text = ex.Message.ToString();
                }
            }
        }
        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }

        private string GetRandomdate(string sAge)
        {
            string retunDate = string.Empty;
            DateTime RandDate = new DateTime();
            if (sAge == "15-24")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/2001", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1992", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1992", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "25-34")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1991", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1982", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1982", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "35-44")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1981", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1972", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1972", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "45-54")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1971", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1962", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1962", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "55-64")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1961", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1952", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1952", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1951", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1930", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1930", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }

            return retunDate;
        }
    }
}