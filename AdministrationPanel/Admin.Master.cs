﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        protected MemberInfo member = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string UserName = this.member.Name;
            string UserType = this.member.Type;
            if (UserType != "SuperAdmin")
            {
                usertab.Visible = false;
            }
        }
        
    }
}