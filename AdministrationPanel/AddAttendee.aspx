﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddAttendee.aspx.cs" Inherits="AdministrationPanel.AddAttendee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LocalScript" runat="server">
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
    <form action="" method="post" id="from1">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <i class="fa ion-card"></i>
                            <h3 class="box-title">Add Attendee</h3>
                        </div>
                        <div class="alert alert-danger" role="alert" id="divError" runat="server" style="display: none; padding: 0px; font-weight: bold;">
                            <asp:Literal ID="litMessage" runat="server" />
                            <span id="msg"></span>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-xs-4">
                                    <!-- TRN number  input -->
                                    <label>Company TRN Number</label>
                                    <asp:RegularExpressionValidator ValidationGroup="info" ID="RegularExpressionValidator1" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtTRNNumber" ValidationExpression="^[0-9]{15,15}$" ErrorMessage="Please enter valid TRN Number" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtTRNNumber" placeholder="Enter your company TRN number" MaxLength="15" CssClass="form-control" />

                                </div>
                                <div class="col-xs-4">
                                    <label>Title<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="rfvTitle" runat="server" SetFocusOnError="true" Display="Dynamic" ControlToValidate="ddlTitle" ErrorMessage="Please select a Title" ForeColor="Red" />
                                    <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                        <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                        <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                        <asp:ListItem Value="Dr.">Dr.</asp:ListItem>
                                        <asp:ListItem Value="Prof.">Prof.</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-xs-4">
                                    <label>First Name<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvFirstName" runat="server" Display="Dynamic" ControlToValidate="txtFirstName" ErrorMessage="Please enter First Name" ForeColor="Red" />
                                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="info" ID="revFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Please enter First Name" ValidationExpression="^[a-zA-Z\s\u0600-\u06FF]+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="First Name" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Last Name<span style="color: red;"> **</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvLastName" runat="server" Display="Dynamic" ControlToValidate="txtLastName" ErrorMessage="Please enter Last Name" ForeColor="Red" />
                                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="info" ID="revLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Please enter Last Name / Surname" ValidationExpression="^[a-zA-Z\s\u0600-\u06FF]+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:TextBox runat="server" ID="txtLastName" placeholder="Last Name" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Position<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvJobTitle" runat="server" Display="Dynamic" ControlToValidate="txtJobTitle" ErrorMessage="Please Enter Position " ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtJobTitle" placeholder="Job Title/ Position" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Company<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvCompany" runat="server" Display="Dynamic" ControlToValidate="txtCompany" ErrorMessage="Please enter Company Name" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtCompany" placeholder="Company/Institution/Organization" CssClass="form-control" />
                                </div>
                                <%--  <div class="col-xs-4">
                                    <label>Address <span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="RequiredFieldValidator8" runat="server" Display="Dynamic" ControlToValidate="txtCompanyAddress" ErrorMessage="Please enter Address" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtCompanyAddress" placeholder="Mailing Address" CssClass="form-control" />
                                </div>--%>
                                <div class="col-xs-4">
                                    <label>Nationality<span style="color: red;"> **</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="RequiredFieldValidator6" SetFocusOnError="true" runat="server" Display="Dynamic" ControlToValidate="ddlNationality" ErrorMessage="Please select a Nationality" ForeColor="Red" />
                                    <div class="field">
                                        <asp:DropDownList runat="server" ID="ddlNationality" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <label>Country of Residence<span style="color: red;"> **</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="rfvCountry" SetFocusOnError="true" runat="server" Display="Dynamic" ControlToValidate="ddlCountry" ErrorMessage="Please select a Country" ForeColor="Red" />
                                    <asp:DropDownList runat="server" ID="ddlCountry" CssClass="form-control" />
                                </div>
                                <%-- <div class="col-xs-4">
                                    <label>State/Province<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator SetFocusOnError="true" ValidationGroup="info" ID="rfvState" runat="server" Display="Dynamic" ControlToValidate="txtState" ErrorMessage="Please Enter State / Province " ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtState" placeholder="State / Province" CssClass="form-control" />
                                </div>--%>
                                <div class="col-xs-4">
                                    <label>City<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="RequiredFieldValidator7" runat="server" Display="Dynamic" ControlToValidate="txtCity" ErrorMessage="Please Enter City" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtCity" placeholder="City" CssClass="form-control" />

                                </div>
                                <%-- <div class="col-xs-4">
                                    <label>Zip / Postal Code<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="txtZip" ErrorMessage="Please Enter Zip / Postal Code " ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtZip" placeholder="Post/Zip/PO Box" CssClass="form-control" />
                                </div>--%>
                                <div class="col-xs-4">
                                    <label>Email<span style="color: red;"> **</span> </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvEmail" runat="server" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid Email Address" ForeColor="Red" />
                                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="info" ID="validEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid Email Address" ValidationExpression="^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9]{2,}(?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+(?:[a-zA-Z]{2,})\b$" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:TextBox runat="server" ID="txtEmail" placeholder="Email" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <!-- Mobile -->
                                    <label>Mobile Number<span style="color: red;"> **</span> </label>
                                    <span id="erroUAEmobile" style="color: red;"></span>
                                    <asp:RegularExpressionValidator ValidationGroup="info" ID="revMobile" runat="server" Display="Dynamic" SetFocusOnError="true" ControlToValidate="txtMobileNumber" ValidationExpression="^[0-9]{8,16}$" ErrorMessage="Please enter valid mobile number (minimum 8)" ForeColor="Red" />
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="RequiredFieldValidator3" runat="server" SetFocusOnError="true" Display="Dynamic" ControlToValidate="ddlMobileCountry" ErrorMessage="Please Select Country Code, " ForeColor="Red" />
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="RequiredFieldValidator4" runat="server" SetFocusOnError="true" Display="Dynamic" ControlToValidate="txtMobileNumber" ErrorMessage="Please enter Mobile Number" ForeColor="Red" />
                                    <div class="field">
                                        <div class="row">
                                            <div class="form-group col-xs-6" style="padding-right: 5px;">
                                                <asp:DropDownList ID="ddlMobileCountry" runat="server" CssClass="form-control" />
                                                <span class="entypo-mobile icon"></span>
                                                <div id="arrow-select-mobile"></div>
                                            </div>
                                            <div class="form-group col-xs-6">
                                                <asp:TextBox runat="server" ID="txtMobileNumber" placeholder="Eg. 501234567" CssClass="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--  <div class="col-xs-4">
                                    <label>Are you a member?<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="RequiredFieldValidator9" runat="server" SetFocusOnError="true" Display="Dynamic" ControlToValidate="rdoDMCCDGJG" ErrorMessage="Please Select" ForeColor="Red" />
                                    <asp:RadioButtonList ID="rdoDMCCDGJG" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>--%>
                                <%--<div class="col-xs-4">
                                    <label>Share my email with other DPMC attendees?<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="True" ID="RequiredFieldValidator10" runat="server" Display="Dynamic" ControlToValidate="rdoSharemyemail" ErrorMessage="Please Select" ForeColor="Red" />
                                    <asp:RadioButtonList ID="rdoSharemyemail" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>--%>
                            </div>
                            <div class="row">
                                <%--<div class="col-xs-4">
                                    <label>What would you like discussed in various panels?<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="True" ID="RequiredFieldValidator11" runat="server" Display="Dynamic" ControlToValidate="txtQuestion" ErrorMessage="Please enter various panels" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtQuestion" placeholder="What would you like discussed in various panels" CssClass="form-control" />
                                </div>--%>
                                <div class="col-xs-4">

                                    <label>Please select your ticket type:<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="True" ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="rdotickettype" ErrorMessage="Please Select" ForeColor="Red" />
                                    <asp:RadioButtonList ID="rdotickettype" runat="server" RepeatDirection="Vertical">
                                        <asp:ListItem Value="A1">Early bird Standard Rate - 2697.45</asp:ListItem>
                                        <asp:ListItem Value="A2">Regular - 3082.80</asp:ListItem> 
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-xs-12">
                                    <label>Comments</label>
                                    <asp:TextBox runat="server" ID="txtComments" placeholder="Comments" Style="width: 100%;" TextMode="MultiLine" />
                                </div>
                            </div> 
                            <div class="box-footer"> 
                                <asp:Button ID="btnRegister" runat="server" ValidationGroup="info" Text="Register And Send Invoice" CssClass="btn btn-info" OnClick="btnRegister_Click" />
                                <asp:Button ID="btnRegisterWithCard" runat="server" ValidationGroup="info" Text="Register And Pay By Card" CssClass="btn btn-info" OnClick="btnRegisterWithCard_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#aspnetForm").submit(function () {
                var selected = $("#ctl00_Body_ddlMobileCountry option:selected").text();
                var myLength = $("#ctl00_Body_txtMobileNumber").val().length;
                if (selected == "United Arab Emirates (971)" && myLength < 9) {
                    $("#ctl00_Body_txtMobileNumber").focus();
                    $("#erroUAEmobile").text("Please enter valid mobile number (minimum 9)");
                    return false;
                }
                else {
                    $("#erroUAEmobile").text("");
                }
            });

            $("#ctl00_Body_txtMobileNumber").focusout(function () {
                var s = $("ctl00_Body_#txtMobileNumber").val();
                $("#ctl00_Body_txtMobileNumber").val(s.replace(/^(0+)/g, ''));
            });

            $("#ctl00_Body_txtMobileNumber").keyup(function () {
                var numbers = $(this).val();
                $(this).val(numbers.replace(/\D/, ''));
            });

            $("#ctl00_Body_ddlCountry").change(function () {
                var selectedIndex = parseInt(this.selectedIndex);
                $('#ctl00_Body_ddlMobileCountry').val(selectedIndex);
                $("#ctl00_Body_txtMobileNumber").val("");
                $("#erroUAEmobile").text("");
                if (selectedIndex == "1") {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 9);

                }
                else {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 15);
                }
            });

            $("#ctl00_Body_ddlMobileCountry").change(function () {
                var selected = $("#ctl00_Body_ddlMobileCountry option:selected").text();
                $("#erroUAEmobile").text("");
                if (selected == "United Arab Emirates (971)") {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").val("");
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 9);
                }
                else {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").val("");
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 15);
                }
            });

        });
    </script>

    <script type="text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnRegister.ClientID %>').prop('disabled', true);
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
</asp:Content>


