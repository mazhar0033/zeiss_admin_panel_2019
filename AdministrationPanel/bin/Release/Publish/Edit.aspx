﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" CodeBehind="Edit.aspx.cs" Inherits="AdministrationPanel.Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script>
        $(document).ready(function () {
            $('#EditBox').find("*").prop("disabled", true);
            $("#Edit").click(function () {
                $('#EditBox').find("*").prop("disabled", false);
                return false;
            });
        });
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="Body" runat="server" ID="Body">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="fa fa-edit"></i>
                        <h3 class="box-title">Edit</h3>
                       <%-- <div class="pull-right box-tools">
                            <a id="Edit" class="btn btn-app">
                                <i class="fa fa-edit"></i>
                                Edit
                            </a>
                        </div>--%>

                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-5">
                                <div id="infoEdit" runat="server" class="alert alert-info alert-dismissable">
                                    <i class="fa fa-info"></i>
                                    <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                                    <asp:Label ID="info" runat="server"></asp:Label>
                                </div>
                            </div>
                        </div>
                            <div class="col-lg-12">
                                <asp:Button ID="btnDownloadInvoice" OnClick="btnDownload_Click" CssClass="btn btn-success" runat="server" Text="Download Abstract Submission" />
                                <asp:Label ID="litMessage" ForeColor="Red" runat="server"></asp:Label>
                            </div>
                    </div>
                    <div id="EditBox" class="box-body">
                        <div class="row">
                            <div class="col-xs-4">
                                <div id="title" class="form-group">
                                    <label id="title1" for="Title">Title </label>
                                    <asp:DropDownList ValidationGroup="form" ID="ddlTitle" runat="server" CssClass="form-control">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="Mr.">Mr.</asp:ListItem>
                                        <asp:ListItem Value="Mrs.">Mrs.</asp:ListItem>
                                        <asp:ListItem Value="Ms.">Ms.</asp:ListItem>
                                        <asp:ListItem Value="Dr.">Dr.</asp:ListItem>
                                        <asp:ListItem Value="Prof.">Prof.</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="firstname" class="form-group">
                                    <label id="Label1" for="FirstName">First Name </label>
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="familyname" class="form-group">
                                    <label class="desc" id="Label2" for="FamilyName">Last Name </label>
                                    <asp:TextBox ID="txtFamilyName" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="country" class="form-group">
                                    <label class="desc" id="Label5" for="Country">Country </label>
                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="nationality" class="form-group">
                                    <label class="desc" id="Label6" for="Nationality">Nationality</label>
                                    <div>
                                        <asp:DropDownList ID="ddlNationality" runat="server" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="City" class="form-group">
                                    <label class="desc" for="PoBox">City </label>
                                    <asp:TextBox ID="txtCity" runat="server" MaxLength="10" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="email" class="form-group">
                                    <label class="desc" id="Label13" for="Email">Email Address</label>
                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="Hospital" class="form-group">
                                    <label class="desc" id="lblHospital" for="Email">Hospital</label>
                                    <asp:TextBox ID="txtHospital" runat="server" CssClass="form-control" />
                                </div>
                            </div>
                            
                            <div class="col-xs-4">
                                <div id="welcome" class="form-group">
                                    <label id="" for="Title">Welcome Dinner </label>
                                    <asp:DropDownList ValidationGroup="form" ID="ddlWelcome" runat="server" CssClass="form-control">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="Business" class="form-group">
                                    <label id="" for="Title">Business Dinner </label>
                                    <asp:DropDownList ValidationGroup="form" ID="ddlBusiness" runat="server" CssClass="form-control">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div id="Hotel" class="form-group">
                                    <label id="" for="Title">Welcome Dinner </label>
                                    <asp:DropDownList ValidationGroup="form" ID="ddlHotel" runat="server" CssClass="form-control">
                                        <asp:ListItem Value=""></asp:ListItem>
                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                        <asp:ListItem Value="No">No</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                    </div>
                        <div class="row box-body" style="display:none">
                            <div class="col-lg-12">
                                <asp:Button ID="btnApprove" OnClick="btnApprove_Click" CssClass="btn btn-success" runat="server" Text="Approve" />
                            </div>
                            <div class="col-lg-12">
                                <asp:Label ID="lblmessage" runat="server" Text="" ForeColor="Red" />
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $(document).ready(function () {

            $("#ctl00_Body_ddlMobileCountry").change(function () {
                var selected = $("#ddlMobileCountry option:selected").text();
                $("#erroUAEmobile").text("");
                if (selected == "971") {
                    $("#ctl00_Body_txtMobileNumber").val("");
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 9);
                }
                else {
                    $("#ctl00_Body_txtMobileNumber").val("");
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 15);
                }
            });

            $("#ctl00_Body_ddlCountry").change(function () {
                var selectedIndex = parseInt(this.selectedIndex);
                $('#ctl00_Body_ddlMobileCountry').val(selectedIndex);
                $("#ctl00_Body_txtMobileNumber").val("");
                $("#erroUAEmobile").text("");
                if (selectedIndex == "1") {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 9);

                }
                else {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 15);
                }
            });

            $("#ctl00_Body_txtMobileNumber").focusout(function () {
                debugger;
                var s = $("#ctl00_Body_txtMobileNumber").val();
                $("#ctl00_Body_txtMobileNumber").val(s.replace(/^(0+)/g, ''));
            });

            $("#ctl00_Body_txtMobileNumber").keyup(function () {
                var numbers = $(this).val();
                $(this).val(numbers.replace(/\D/, ''));
            });
        });

    </script>


</asp:Content>
