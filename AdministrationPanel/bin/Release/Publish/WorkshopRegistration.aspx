﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" CodeBehind="WorkshopRegistration.aspx.cs" Inherits="AdministrationPanel.WorkshopRegistration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LocalScript" runat="server">
    <script type="text/javascript" src="js/bs.pagination.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="js/distrib.min.js"></script>
    <script type="text/javascript" src="js/easybox.min.js"></script>
    <link rel="stylesheet" href="css/dark/easybox.min.css" type="text/css" media="screen" />
    <script type="text/javascript">
        
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server" ID="Body">
    <section class="content-header">
        <h1>Workshop Registration
                       
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="default.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Workshop Registration</li>
        </ol>
    </section>
    <section class="content">
        
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <i class="fa ion-card"></i>
                        <h3 class="box-title">Workshop Registration</h3>
                        <div>
                            <asp:Label Style="position: absolute; top: 0; right: 0; margin: 8px 20px 2px 4px; color: #0099CC; font-size: 18px;" Font-Bold="true"
                                ID="totalCount" runat="server" />
                        </div>
                    </div>
                    <div id="gridReg" runat="server" class="box-body table-responsive">
                        <div id="registration" class="dataTables_wrapper form-inline" role="grid">

                            <asp:GridView runat="server" PageSize="10" PagerStyle-CssClass="bs-pagination" CssClass="table table-bordered table-striped dataTable"
                                ID="gvRegistrations"  DataKeyNames="WID"  AutoGenerateColumns="false" OnPageIndexChanging="gvRegistrations_PageIndexChanging"  AllowPaging="True">
                                <Columns>
                                    <asp:BoundField DataField="WorkShopName" HeaderText="WorkShop Name" />
                                    <asp:BoundField DataField="WorkShopCode" HeaderText="WorkShop Code" />
                                    <asp:BoundField DataField="MaxCount" HeaderText="Total Capacity" />
                                     <asp:BoundField DataField="RegCount" HeaderText="Registered Seats" />
                                    <asp:BoundField DataField="Remaing" HeaderText="Remaining Seats" />
                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                    <div class="box-footer">

                        <asp:LinkButton OnClick="btnGenerateCSV_Click" ID="btnGenerateCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-
download"></i>&nbsp;Generate CSV</asp:LinkButton>
                    </div>

                </div>
            </div>
        </div>
    </section>
</asp:Content>
