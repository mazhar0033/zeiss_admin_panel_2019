﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AdministrationPanel.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/style.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">

	<form id="Form1" runat="server" name="login-form" class="login-form" method="post">
	
		<div class="header">
		<h1>Login Form</h1>
            
		</div>
	
		<div class="content">
		<asp:TextBox runat="server" ID="txtUsername" name="username" type="text" class="input username" placeholder="Username" />
		<div class="user-icon"></div>
		<asp:TextBox runat="server" ID="txtPassword" TextMode="Password" class="input password" placeholder="Password" />
		<div class="pass-icon"></div>		
        <asp:CheckBox runat="server" CssClass="" ID="chkRemember" Text="Remember Me" style="margin-top:15px;" />
		</div>

		<div class="footer">
            
		<asp:Button runat="server" ID="btnLogin" type="submit" name="submit" Text="Login" class="button" OnClick="btnLogin_Click" />
		</div>
	    <span style="color:red;font-size:medium;line-height:20px;"><asp:Label ID="litMessage" runat="server" /></span>
	</form>

</div>
<div class="gradient"></div>
</body>
</html>
