﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" CodeBehind="registrations.aspx.cs" Inherits="AdministrationPanel.registrations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LocalScript" runat="server">
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    <script type="text/javascript" src="js/bs.pagination.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="js/distrib.min.js"></script>
    <script type="text/javascript" src="js/easybox.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script type="text/javascript">
        $(document).ready(function () {
            $("#ctl00_Body_txtdate").datepicker();
        });
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server" ID="Body">
    <!-- COMPOSE MESSAGE MODAL -->
    <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-envelope-o"></i>Compose New Message</h4>
                </div>
                <form action="#" method="post">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Subject:</span>
                                <asp:TextBox ID="txtEmailSubject" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea class="textarea" runat="server" id="txtEmailMessage" placeholder="Message" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer clearfix">

                        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Discard</button>

                        <asp:LinkButton ID="btnEmailSubmit" OnClick="btnEmailSubmit_Click" runat="server" CssClass="btn btn-primary pull-left"><i class="fa fa-

envelope"></i> Send Message</asp:LinkButton>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <%--Main Section--%>
    <section class="content-header">
        <h1>Visitor Registrations
                       
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="default.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Registrations</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="alert alert-danger" role="alert" id="divError" runat="server" style="display: none; padding: 20px; font-weight: bold;">
                        <asp:Literal ID="litMessage" runat="server" />
                        <span id="msg"></span>
                    </div>
                    <div class="box-header">
                        <i class="fa ion-card"></i>
                        <h3 class="box-title">Search</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            <div class="col-xs-4">
                                <label>First Name</label>
                                <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <label>Last Name</label>
                                <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <label>Country</label>
                                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <label>Nationality</label>
                                <asp:DropDownList ID="ddlNationality" runat="server" CssClass="form-control" />
                            </div>

                            <div class="col-xs-4">
                                <label>Email</label>
                                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <label>Welcome Dinner</label>
                                <asp:DropDownList ID="ddlWelcome" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="">-Select a value-</asp:ListItem>
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-4">
                                <label>Business Dinner</label>
                                <asp:DropDownList ID="ddlBusiness" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="">-Select a value-</asp:ListItem>
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-xs-4">
                                <label>Hotel Reservation</label>
                                <asp:DropDownList ID="ddlHotel" runat="server" CssClass="form-control">
                                    <asp:ListItem Value="">-Select a value-</asp:ListItem>
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-xs-4">
                                <label>Date</label>
                                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control" autocomplete="off" />
                            </div>

                        </div>
                        <div class="box-footer">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-info" OnClick="btnSearch_Click" />
                             <div style="text-align: right;">
                                <asp:Button ID="btnDownloadInvoice" runat="server" Text="Download All Abstract Submission" CssClass="btn btn-info" OnClick="btnDownloadInvoice_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <i class="fa ion-card"></i>
                        <h3 class="box-title">Registrations</h3>
                        <div>
                            <asp:Label Style="position: absolute; top: 0; right: 0; margin: 8px 20px 2px 4px; color: #0099CC; font-size: 18px;" Font-Bold="true"
                                ID="totalCount" runat="server" />
                        </div>
                    </div>
                    <div id="gridReg" runat="server" class="box-body table-responsive" style="overflow-x: auto;">
                        <div id="registration" class="dataTables_wrapper form-inline" role="grid">
                            <asp:GridView runat="server" PageSize="10" PagerStyle-CssClass="bs-pagination" CssClass="table table-bordered table-striped dataTable"
                                ID="gvRegistrations" DataKeyNames="ID" AutoGenerateColumns="false" OnPageIndexChanging="gvRegistrations_PageIndexChanging" AllowPaging="True">
                                <Columns>
                                    <%--<asp:HyperLinkField HeaderText="Full Name" DataTextField="Name" DataNavigateUrlFields="ID" DataNavigateUrlFormatString="edit.aspx?id={0}" />--%>
                                    <asp:BoundField DataField="Name" HeaderText="Full Name" />
                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                    <asp:BoundField DataField="Country" HeaderText="Country" />
                                    <asp:BoundField DataField="Nationality" HeaderText="Nationality" />
                                    <asp:BoundField DataField="Hospital" HeaderText="Hospital" />
                                    <asp:BoundField DataField="City" HeaderText="City" />
                                    <asp:BoundField DataField="WelcomeDinner" HeaderText="Welcome Dinner" />
                                    <asp:BoundField DataField="BusinessDinner" HeaderText="Business Dinner" />
                                    <asp:BoundField DataField="HotelReservation" HeaderText="Hotel Reservation" />

                                    <%--<asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDownloadAbstract" runat="server" Text="Abstract Submission Form"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDownload" runat="server" CommandArgument='<%#Eval("ID")%>' OnClick="btnDownload_Click" Style="background-image: url('img/downloads-icon.png'); background-repeat: no-repeat; width: 24px; height: 24px; border: 0;" ToolTip="Download Abstract Submission Form" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                    <div class="box-footer">

                        <asp:LinkButton OnClick="btnGenerateCSV_Click" ID="btnGenerateCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Generate CSV</asp:LinkButton>
                        <%--   <a class="btn btn-primary" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil"></i>Compose Message</a>--%>
                    </div>

                </div>
            </div>
        </div>
    </section>

</asp:Content>
