﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Admin.Master" CodeBehind="default.aspx.cs" Inherits="AdministrationPanel._default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

    <script>
        $(function () {
            var value = document.getElementById('<%=txtResult.ClientID%>').value;
            value = JSON.parse(value);
            var visitorsData = value;
            //World map by jvectormap
            $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "#fff",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    }
                },
                series: {
                    regions: [{
                        values: visitorsData,
                        scale: ["#3c8dbc", "#2D79A6"], //['#3E5E6B', '#A6BAC2'],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionLabelShow: function (e, el, code) {
                    if (typeof visitorsData[code] != "undefined")
                        el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
                },
                onRegionClick: function (event, code) {
                    var map = $('#world-map').vectorMap('get', 'mapObject');
                    var name = map.getRegionName(code);
                    if (visitorsData[code] != null) {
                        window.location.href = "registrations.aspx?country=" + name;
                    }
                },
            });
        });



    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="Body" runat="server" ID="Body">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <section class="content-header">

        <h1>Dashboard                       
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
         <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 runat="server" id="totalCounts">
                            
                        </h3>
                        <p>
                            Total Registrations
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 runat="server" id="TodayCounts">
                            
                        </h3>
                        <p>
                            Today Registrations
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 runat="server" id="YestCounts">
                            
                        </h3>
                        <p>
                            Yesterday Registrations
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 runat="server" id="WeekCount">
                            
                        </h3>
                        <p>
                             Last Week Registrations
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xs-6" style="display:none">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 runat="server" id="MonthCount">
                            
                        </h3>
                        <p>
                             Last Month Registrations
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">Visitors
                        </h3>
                    </div>
                    <div class="box-body no-padding">
                        <asp:TextBox ID="txtResult" runat="server" Style="display: none;" />
                        <div id="world-map" style="height: 300px;"></div>
                    </div>
                    <div class="box-body no-padding">
                        <p style="font-weight: bold; text-decoration: underline;">
                            Country wise registration analysis
                        </p>
                        <div class="row">
                            <!---->
                            <div class="col-lg-6 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-fuchsia">
                                    <div class="inner">
                                        <h3 runat="server" id="totalLocalRegistrationCounts"></h3>
                                        <p>
                                            Local Registration (based in UAE)
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-xs-6">
                                <!-- small box -->
                                <div class="small-box bg-teal">
                                    <div class="inner">
                                        <h3 runat="server" id="totalInternationalRegistrationCounts"></h3>
                                        <p>
                                            International Registration
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <p style="font-weight: bold; text-decoration: underline;">
                            Top 5 Countries registration analysis
                        </p>
                        <div class="table-responsive">
                            <asp:GridView ID="gvCountry" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                <Columns>
                                    <asp:HyperLinkField HeaderText="Top 5 Countries" DataTextField="Country" DataNavigateUrlFields="Country" DataNavigateUrlFormatString="registrations.aspx?country={0}" />
                                    <asp:BoundField DataField="Count" HeaderText="Count" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="box-footer">
                            <asp:LinkButton OnClick="btnGenerateCSV_Click" ID="btnGenerateCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Generate CSV</asp:LinkButton>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <p style="font-weight: bold; text-decoration: underline;">
                            GCC Countries wise registration analysis
                        </p>
                        <div class="table-responsive">
                            <asp:GridView ID="gvGCCCountry" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                <Columns>
                                    <asp:HyperLinkField HeaderText="GCC Countries" DataTextField="Country" DataNavigateUrlFields="Country" DataNavigateUrlFormatString="registrations.aspx?country={0}" />
                                    <asp:BoundField DataField="Count" HeaderText="Count" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="box-footer">
                            <asp:LinkButton OnClick="btnGCCCountryCSV_Click_Click" ID="btnGCCCountryCSV_Click" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Generate CSV</asp:LinkButton>
                        </div>
                    </div>
                    <div class="box-body no-padding">
                        <p style="font-weight: bold; text-decoration: underline;">
                            Top 5 Nationality registration analysis
                        </p>
                        <div class="table-responsive">
                            <asp:GridView ID="gvNationality" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                                <Columns>
                                    <asp:HyperLinkField HeaderText="Top 5 Nationality" DataTextField="Nationality" DataNavigateUrlFields="Nationality" DataNavigateUrlFormatString="registrations.aspx?nationality={0}" />
                                    <asp:BoundField DataField="Count" HeaderText="Count" />
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="box-footer">
                            <asp:LinkButton OnClick="btnNationalityCSV_Click" ID="btnNationalityCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Generate CSV</asp:LinkButton>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="fa fa-map-marker"></i>
                        <h3 class="box-title">2019 Weekly Registrations
                        </h3>
                    </div>
                    <div class="box-body no-padding">
                        <div class="table-responsive" style="overflow: auto;">

                            <asp:LineChart ID="LineChart1" runat="server"
                                ChartWidth="450" ChartHeight="300" ChartType="Stacked"
                                ChartTitleColor="#0E426C" CategoryAxisLineColor="#D08AD9"
                                ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB">
                            </asp:LineChart>
                        </div>
                    </div>
                    <div class="box-footer">
                        <asp:LinkButton OnClick="btnDailyReportCSV_Click" ID="btnDailyReportCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Daily Report CSV</asp:LinkButton>
                    </div>
                   <%-- <div class="box box-primary">
                        <div class="box-header">
                            <i class="fa fa-map-marker"></i>
                            <h3 class="box-title">2018 Weekly Registrations
                            </h3>
                        </div>
                        <div class="box-body no-padding">
                            <div class="table-responsive" style="overflow: auto;">
                                <asp:LineChart ID="LineChart2" runat="server"
                                    ChartWidth="450" ChartHeight="300" ChartType="Stacked"
                                    ChartTitleColor="#0E426C" CategoryAxisLineColor="#D08AD9"
                                    ValueAxisLineColor="#D08AD9" BaseLineColor="#A156AB">
                                </asp:LineChart>
                            </div>
                        </div>
                    </div>--%>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="box-body no-padding">
                    <p style="font-weight: bold; text-decoration: underline;">
                        Last 5 days registration analysis
                    </p>
                    <div class="table-responsive">
                        <asp:GridView ID="gvDays" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                            <Columns>
                                <asp:BoundField HeaderText="Last 5 Days" DataField="Date" DataFormatString="{0:d}" />
                                <asp:BoundField DataField="Count" HeaderText="Count" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="box-footer">
                        <asp:LinkButton OnClick="btnDailyListReportCSV_Click" ID="btnDailyListReportCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Daily Report CSV</asp:LinkButton>
                    </div>
                </div>
                <div class="box-body no-padding" style="display:none">
                    <p style="font-weight: bold; text-decoration: underline;">
                        Badge category wise registration analysis
                    </p>
                    <div class="table-responsive" style="overflow: auto; display: none;">
                        <asp:PieChart ID="PieChartCategory" runat="server" ChartWidth="450" ChartHeight="300" ChartTitleColor="#0E426C"></asp:PieChart>
                    </div>
                    <div class="table-responsive">
                        <asp:GridView ID="gvCategory" BorderWidth="0" GridLines="None" AutoGenerateColumns="false" runat="server" CssClass="table table-striped">
                            <Columns>
                                <asp:HyperLinkField HeaderText="Category" DataTextField="RegistrationType" DataNavigateUrlFields="RegistrationType" DataNavigateUrlFormatString="registrations.aspx?Category={0}" />
                                <asp:BoundField DataField="Count" HeaderText="Count" />
                            </Columns>
                        </asp:GridView>
                    </div>
                    <div class="box-footer">
                        <asp:LinkButton OnClick="btnCategoryCSV_Click" ID="btnCategoryCSV" runat="server" CssClass="btn btn-info"><i class="fa fa-download"></i>&nbsp;Generate CSV</asp:LinkButton>
                    </div>
                </div>
            </div>
        </div>
        <div>
        </div>
    </section>
</asp:Content>
