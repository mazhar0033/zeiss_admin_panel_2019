﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace AdministrationPanel
{
    /// <summary>
    /// Summary description for CountryRegistrations
    /// </summary>
    public class CountryRegistrations : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string test = ConvertDataTabletoString();

            context.Response.Clear();
            context.Response.Write(test.ToString());
        }

        public string ConvertDataTabletoString()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                using (SqlCommand cmd = new SqlCommand("select Country,Count(*) As Count from Visitor group by country", con))
                {
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    string json = JsonConvert.SerializeObject(dt, new Newtonsoft.Json.Formatting());
                    json = "\"countries\":" + json;
                    return json;
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}