﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace AdministrationPanel
{
    public class MemberInfo
    {
        public int ID { get; private set; }
        public string Name { get; private set; }
        public string Type { get; private set; }

        public MemberInfo(string username)
        {
            this.Load(username);
        }


        public MemberInfo(int id, string name)
        {
            this.ID = id;
            this.Name = name;
            // this._initialized = true;
        }

        private void Load(string username)
        {
            string sql = @"SELECT * FROM Administrators WHERE Username=@Username";
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conn))
                {
                    cmd.CommandTimeout = 500;

                    cmd.Parameters.AddWithValue("@Username", username);

                    SqlDataReader reader = cmd.ExecuteReader();
                    bool isExist = reader.Read();

                    if (isExist)
                    {
                        this.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                        this.Name = reader.GetString(reader.GetOrdinal("Name"));
                        this.Type = reader.GetString(reader.GetOrdinal("Type"));
                    }

                    reader.Close();

                    if (!isExist)
                        throw new Exception("Invalid agent info");
                }
            }
        }

    }
}
