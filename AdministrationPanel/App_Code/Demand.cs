﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Demand
/// </summary>
public class Demand
{
    public string priceTypeCode { get; set; }
    public int quantity { get; set; }
    public int admits { get; set; }
    public string offerCode { get; set; }
    public string qualifierCode { get; set; }
    public string entitlement { get; set; }
    public Customer customer { get; set; }
}