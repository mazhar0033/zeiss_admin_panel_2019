﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Customer
/// </summary>
public class Customer
{
    public string lastname { get; set; }
    public string nationality { get; set; }
    public string email { get; set; }
    public string dateofbirth { get; set; }
    public string internationalcode { get; set; }
    public string areacode { get; set; }
    public string phonenumber { get; set; }
    public string city { get; set; }
    public string state { get; set; }
    public string countrycode { get; set; }
}




