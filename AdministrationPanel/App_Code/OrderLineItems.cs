﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Barcodes
/// </summary>
public class Orders
{
    public int id { get; set; }
    public string PriceTypeCode { get; set; }
    public string PriceCategoryCode { get; set; }
    public string PriceTypeName { get; set; }
    public string Barcode { get; set; }
    public string OrderID { get; set; }
}