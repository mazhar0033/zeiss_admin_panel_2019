﻿using OnlineRegistration.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for DTCMAPI
/// </summary>
public class DTCMAPI
{
    #region Method
    public string Oauth()
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/oauth2/accesstoken";
        HttpWebResponse response = null;
        try
        {
            // Create the data to send
            StringBuilder data = new StringBuilder();
            data.Append("client_id=" + ConfigurationManager.AppSettings["client_id"]);
            data.Append("&client_secret=" + ConfigurationManager.AppSettings["client_secret"]);
            data.Append("&grant_type=client_credentials");
            byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());

            // Setup the Request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            // Write data
            Stream postStream = request.GetRequestStream();
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            // Send Request & Get Response
            response = (HttpWebResponse)request.GetResponse();

            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string json = reader.ReadLine();
                //// Retrieve and Return the Access Token
                //JavaScriptSerializer ser = new JavaScriptSerializer();
                //Dictionary<string, object> x = (Dictionary<string, object>)ser.DeserializeObject(json);
                result = json;

            }

            AppLogger.AppLoggerEntry("token : Not found in cache -> Fetched from API");
            return result;
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "Oauth", ex, "HttpPost");
            throw ex;
        }
    }
    public string GetPerformancePrice(string accessToken)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/performances/" + ConfigurationManager.AppSettings["performancecode"].ToString() + "/prices?channel=W&sellerCode=" + ConfigurationManager.AppSettings["sellerCode"].ToString();
        StringBuilder data = new StringBuilder();
        data.Append("Authorization: Bearer " + accessToken);
        try
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add(data.ToString());
            request.ContentType = "application/json";
            using (WebResponse response1 = request.GetResponse())
            {
                using (Stream stream = response1.GetResponseStream())
                {
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                    result = readStream.ReadToEnd();
                }
            }
            return result;

        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GetPerformancePrice", ex, "HttpGet");
            throw ex;
        }
    }
    public string GetPerformanceAvailability(string accessToken)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/performances/" + ConfigurationManager.AppSettings["performancecode"].ToString() + "/availabilities?channel=W&sellerCode=" + ConfigurationManager.AppSettings["sellerCode"].ToString();
        StringBuilder data = new StringBuilder();
        data.Append("Authorization: Bearer " + accessToken);
        try
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add(data.ToString());
            request.ContentType = "application/json";
            using (WebResponse response1 = request.GetResponse())
            {
                using (Stream stream = response1.GetResponseStream())
                {
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                    result = readStream.ReadToEnd();
                }
            }
            return result;

        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GetPerformanceAvailability", ex, "HttpGet");
            throw ex;
        }
    }
    public string GetPerformanceSeatAvailability(string accessToken, string Section)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/performances/" + ConfigurationManager.AppSettings["performancecode"].ToString() + "/sections/" + "S" + Section + "/?channel=W&sellerCode=" + ConfigurationManager.AppSettings["sellerCode"].ToString();
        StringBuilder data = new StringBuilder();
        data.Append("Authorization: Bearer " + accessToken);
        try
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add(data.ToString());
            request.ContentType = "application/json";
            using (WebResponse response1 = request.GetResponse())
            {
                using (Stream stream = response1.GetResponseStream())
                {
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                    result = readStream.ReadToEnd();
                }
            }
            return result;

        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GetPerformanceAvailability", ex, "HttpGet");
            throw ex;
        }
    }
    public string GetOrderReserver(string accessToken, string OrderId, string NetPrice)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/orders/" + OrderId + "/reverse";
        HttpWebResponse response = null;
        string data = "{\"Seller\":\"" + ConfigurationManager.AppSettings["sellerCode"].ToString() + "\",\"orderId\":\"" + OrderId + "\",\"refunds\": [{\"Amount\":\"" + NetPrice + "\",\"MeansOfPayment\":\"EXTERNAL\"}]}";
        byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("Authorization: Bearer " + accessToken);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            Stream postStream = request.GetRequestStream();
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string json = reader.ReadLine();
                result = json;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GetOrderReserver", ex, "HttpGet");
            throw ex;
        }
        return result;
    }
    public string GenerateBasket(string accessToken, string list_Demand, string Area)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/baskets";
        HttpWebResponse response = null;
        string data = string.Empty;

        data = "{\"Channel\":\"W\",\"Seller\":\"" + ConfigurationManager.AppSettings["sellerCode"].ToString() + "\",\"Performancecode\":\"" + ConfigurationManager.AppSettings["performancecode"].ToString() + "\",\"Area\":\"" + Area + "\",\"Demand\":" + list_Demand + ",\"Fees\":[{\"Type\":5,\"Code\":\"W\"}]}";

        byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("Authorization: Bearer " + accessToken);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            Stream postStream = request.GetRequestStream();
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string json = reader.ReadLine();
                result = json;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GenerateBasked", ex, "HttpPost");
            throw ex;
        }
        return result;
    }
    public string GenerateCoustomer(string accessToken, string Coustomer)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/customers?sellerCode=" + ConfigurationManager.AppSettings["sellerCode"].ToString() + "";
        HttpWebResponse response = null;
        string data = Coustomer;
        byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("Authorization: Bearer " + accessToken);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            Stream postStream = request.GetRequestStream();
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string json = reader.ReadLine();
                result = json;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GenerateCoustomer", ex, "HttpPost");
            throw ex;
        }
        return result;
    }
    public string PurchaseBasked(string accessToken, string BasketId, int Cid, int accountno, string name, string Amount)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/Baskets/" + BasketId + "/purchase";
        HttpWebResponse response = null;
        string data = "{\"Seller\":\"" + ConfigurationManager.AppSettings["sellerCode"].ToString() + "\",\"customer\":{\"ID\":" + Cid + ",\"Account\":" + accountno + ",\"AFile\":\"" + name + "\"},\"Payments\":[{\"Amount\":" + Amount + ",\"MeansOfPayment\":\"EXTERNAL\"}]}";
        byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.Headers.Add("Authorization: Bearer " + accessToken);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;


            Stream postStream = request.GetRequestStream();
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();
            response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string json = reader.ReadLine();
                result = json;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "PurchaseBasked", ex, "HttpPost");
            throw ex;
        }
        return result;
    }
    public string GetOrderView(string accessToken, string ordernumber)
    {
        string result = string.Empty;
        string url = "https://api.etixdubai.com/orders/" + ordernumber + "?sellerCode=" + ConfigurationManager.AppSettings["sellerCode"].ToString() + "";
        StringBuilder data = new StringBuilder();
        data.Append("Authorization: Bearer " + accessToken);
        try
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add(data.ToString());
            request.ContentType = "application/json";
            using (WebResponse response1 = request.GetResponse())
            {
                using (Stream stream = response1.GetResponseStream())
                {
                    StreamReader readStream = new StreamReader(stream, Encoding.UTF8);
                    result = readStream.ReadToEnd();
                }
            }
            return result;

        }
        catch (Exception ex)
        {
            ErrorLogger.Log("DTCMAPI.cs", "GetOrderView", ex, "HttpGet");
            throw ex;
        }
    }
    #endregion
}