﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class BasketPurchase
{
    public string Seller { get; set; }
    public CustomerDetail customer { get; set; }
    public List<Payment> Payments { get; set; }
}
