﻿using OnlineRegistration.Logger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

public class SalesForce
{
    public OAuth2Token GetOAuthToken()
    {
        string result = string.Empty;
        string url = ConfigurationManager.AppSettings["OAuthToken"].ToString();
        string data = string.Empty;

        //data = "{\"client_id\":\"" + ConfigurationManager.AppSettings["client_id"].ToString() + "\",\"client_secret\":\"" + ConfigurationManager.AppSettings["performancecode"].ToString() + "\",\"grant_type\":\"" + ConfigurationManager.AppSettings["grant_type"].ToString() + "\",\"username\":\"" + ConfigurationManager.AppSettings["username"].ToString() + "\",\"password\":\"" + ConfigurationManager.AppSettings["password"].ToString() + "\"}";

        data = @"client_id=" + ConfigurationManager.AppSettings["client_id"].ToString() + "&"
               + "client_secret=" + ConfigurationManager.AppSettings["client_secret"].ToString() + "&"
               + "grant_type=" + ConfigurationManager.AppSettings["grant_type"].ToString() + "&"
               + "username=" + ConfigurationManager.AppSettings["username"].ToString() + "&"
               + "password=" + ConfigurationManager.AppSettings["password"].ToString();
        try
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            MyWebRequest myRequest = new MyWebRequest(url, "POST", data);
            result = myRequest.GetResponse();
            JavaScriptSerializer json_serializer = new JavaScriptSerializer();
            OAuth2Token routes_list = json_serializer.Deserialize<OAuth2Token>(result);
            return routes_list;
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("SalesForce.cs", "GetOAuthToken", ex, "HttpPost");
            throw ex;
        }

    }

    public string DMCC_ProlianceServices_API(string accessToken, string TransactionId, string Amount, string CustomerName, string VendorEmailId, string Remarks, string EventName)
    {
        HttpWebResponse response = null;
        string result = string.Empty;
        string url = ConfigurationManager.AppSettings["ProlianceServices_API"].ToString();
        string data = "{\"request\":  {\"TransactionId\":\"" + TransactionId + "\",\"Amount\":\"" + Amount + "\",\"CustomerName\":\"" + CustomerName + "\",\"VendorEmailId\":\"" + VendorEmailId + "\",\"EventName\":\"" + EventName + "\",\"Remarks\":\"" + Remarks + "\",\"AccountNumber\":\"" + ConfigurationManager.AppSettings["AccountNumber"].ToString() + "\" ,\"ReturnURL\":\"" + ConfigurationManager.AppSettings["ReturnURL"].ToString() + "\" ,\"ServiceId\":\"" + ConfigurationManager.AppSettings["ServiceId"].ToString() + "\"} }";
        byte[] byteArray = Encoding.UTF8.GetBytes(data.ToString());
        
        try
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.PreAuthenticate = true;
            request.Headers.Add("Authorization: Bearer " + accessToken);
            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            Stream postStream = request.GetRequestStream();
            postStream.Write(byteArray, 0, byteArray.Length);
            postStream.Close();

            response = (HttpWebResponse)request.GetResponse();
            using (StreamReader reader = new StreamReader(response.GetResponseStream()))
            {
                string json = reader.ReadLine();
                result = json;
                return result;
            }
        }
        catch (Exception ex)
        {
            ErrorLogger.Log("SalesForce.cs", "DMCC_ProlianceServices_API", ex, "HttpPost");
            throw ex;
        } 
    } 

    public class MyWebRequest
    {
        private WebRequest request;
        private Stream dataStream;

        private string status;

        public String Status
        {
            get
            {
                return status;
            }
            set
            {
                status = value;
            }
        }

        public MyWebRequest(string url)
        {
            // Create a request using a URL that can receive a post.

            request = WebRequest.Create(url);
        }

        public MyWebRequest(string url, string method)
            : this(url)
        {

            if (method.Equals("GET") || method.Equals("POST"))
            {
                // Set the Method property of the request to POST.
                request.Method = method;
            }
            else
            {
                throw new Exception("Invalid Method Type");
            }
        }

        public MyWebRequest(string url, string method, string data)
            : this(url, method)
        {

            // Create POST data and convert it to a byte array.
            string postData = data;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            // Close the Stream object.
            dataStream.Close();

        }

        public string GetResponse()
        {
            // Get the original response.
            WebResponse response = request.GetResponse();

            this.Status = ((HttpWebResponse)response).StatusDescription;

            // Get the stream containing all content returned by the requested server.
            dataStream = response.GetResponseStream();

            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);

            // Read the content fully up to the end.
            string responseFromServer = reader.ReadToEnd();

            // Clean up the streams.
            reader.Close();
            dataStream.Close();
            response.Close();

            return responseFromServer;
        }

    }

    public class Request
    {
        public string TransactionId { get; set; }
        public int Amount { get; set; }
        public string AccountNumber { get; set; }
    }

    public class SalesforceReceiptRequest
    {
        public Request Request { get; set; }
    }

    public class OAuth2Token
    {
        public string access_token { get; set; }
        public string instance_url { get; set; }
        public string id { get; set; }
        public string token_type { get; set; }
        public string issued_at { get; set; }
        public string signature { get; set; }
    }
}

