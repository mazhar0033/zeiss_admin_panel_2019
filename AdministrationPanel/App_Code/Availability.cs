﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Availability
/// </summary>
public class Availability
{
    public bool SoldOut { get; set; }
    public string StatusCode { get; set; }
}