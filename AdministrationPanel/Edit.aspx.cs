﻿using iText.Html2pdf;
using OnlineRegistration.Logger;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class Edit : System.Web.UI.Page
    {
        private int EditID = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                Response.Write("Invalid ID");
                Response.End();
            }

            try
            {
                this.EditID = Convert.ToInt32(Request.QueryString["id"]);
            }
            catch
            {
                Response.Write("Invalid ID");
                Response.End();
            }
            if (!IsPostBack)
            {
               GetInfo();
                PopulateCountry();
             //   PopulateCountryCodes();
                PopulateForm();
            }
        }

        private void GetInfo()
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandTimeout = 500;
                    cmd.CommandText = @"Select TOP 1 * from EditLogs where VisitorId =" + this.EditID + " order by DateUpdated desc";
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            string sDateTime = reader["DateUpdated"].ToString();
                            info.Text = "Last Updated on " + sDateTime;
                        }
                        else
                            infoEdit.Attributes.Add("style", "display:none");
                    }
                }
            }
        }

        private void PopulateForm()
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandTimeout = 500;
                    cmd.Connection = con;
                    string sql = "Select * from Registrations WHERE ID=@ID";
                    cmd.CommandText = sql;
                    cmd.Parameters.AddWithValue("@ID", this.EditID);

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        ddlTitle.SelectedItem.Text = reader.GetString(reader.GetOrdinal("Title"));
                        txtFirstName.Text = reader.GetString(reader.GetOrdinal("FirstName"));
                        txtFamilyName.Text = reader.GetString(reader.GetOrdinal("LastName"));
                        ddlCountry.SelectedItem.Text = reader.GetString(reader.GetOrdinal("Country"));
                        ddlNationality.SelectedItem.Text = reader.GetString(reader.GetOrdinal("Nationality"));
                        txtEmail.Text = reader.GetString(reader.GetOrdinal("Email"));
                        txtCity.Text = reader.GetString(reader.GetOrdinal("City"));
                        txtHospital.Text = reader.GetString(reader.GetOrdinal("hospital"));
                        if (reader.GetString(reader.GetOrdinal("WelcomeDinner")) == "Yes")
                            ddlWelcome.SelectedValue= "Yes";
                        else
                            ddlWelcome.SelectedValue = "No";
                        if (reader.GetString(reader.GetOrdinal("BusinessDinner")) == "Yes")
                            ddlBusiness.SelectedValue = "Yes";
                        else
                            ddlBusiness.SelectedValue = "No";

                        if (reader.GetString(reader.GetOrdinal("hotelreservation")) == "Yes")
                            ddlHotel.SelectedValue = "Yes";
                        else
                            ddlHotel.SelectedValue = "No";
                    }
                }
            }
        }

        private void PopulateCountryCodes()
        {
            DataTable country = new DataTable();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT ID, CountryCode FROM Country", con);
                    adapter.Fill(country);

                    ddlCountry.DataSource = country;
                    ddlCountry.DataTextField = "CountryCode";
                    ddlCountry.DataValueField = "ID";
                    ddlCountry.DataBind();

                    ddlNationality.DataSource = country;
                    ddlNationality.DataTextField = "CountryCode";
                    ddlNationality.DataValueField = "ID";
                    ddlNationality.DataBind();

                }
                catch (Exception ex)
                {
                    // Handle the error
                }
            }
            // Add the initial item - you can add this even if the options from the
            // db were not successfully loaded
            ddlCountry.Items.Insert(0, new ListItem(string.Empty, "0"));
            ddlNationality.Items.Insert(0, new ListItem(string.Empty, "0"));
        }

        public void PopulateCountry()
        {
            DataTable country = new DataTable();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT ID, CountryCode,Country FROM Country", con);
                    adapter.Fill(country);

                    ddlNationality.DataSource = country;
                    ddlNationality.DataTextField = "Country";
                    ddlNationality.DataValueField = "ID";
                    ddlNationality.DataBind();

                    ddlCountry.DataSource = country;
                    ddlCountry.DataTextField = "Country";
                    ddlCountry.DataValueField = "CountryCode";
                    ddlCountry.DataBind();

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            ddlNationality.Items.Insert(0, new ListItem(string.Empty, ""));
            ddlCountry.Items.Insert(0, new ListItem(string.Empty, ""));
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            UpdateEditLogs();
            insertDetails();
        }
        protected MemberInfo member = null;
        private void UpdateEditLogs()
        {
            int sVisitorID = this.EditID;
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string sUserName = this.member.Name;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandText = @"insert into EditLogs(VisitorId,DateUpdated,Username) values(@VisitorId,@DateUpdated,@Username)";
                    cmd.Parameters.Add(new SqlParameter("@VisitorId", sVisitorID));
                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@Username", sUserName));
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
        }

        private void insertDetails()
        {
            string sTitle = ddlTitle.SelectedValue;
            string sFirstName = txtFirstName.Text;
            string sFamilyName = txtFamilyName.Text;
            string sCountry = ddlCountry.SelectedItem.Text;
            string sNationality = ddlNationality.SelectedItem.Text;
            string sCity = txtCity.Text;
            string sMobileCountry = ddlCountry.SelectedItem.Text;
            string sEmail = txtEmail.Text;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandText = @"Update Ev_Registration Set
                                    Title = @Title,
                                    FirstName = @FirstName,
                                    FamilyName = @FamilyName,
                                    Company = @Company,
                                    JobTitle = @JobTitle,
                                    Country = @Country,
                                    Nationality = @Nationality,
                                    Address = @Address,
                                    PoBox = @PoBox,
                                    City = @City,
                                    OfficeCountry = @OfficeCountry,
                                    OfficeNumber = @OfficeNumber,
                                    FaxCountry = @FaxCountry,
                                    Fax = @Fax,
                                    MobileCountry = @MobileCountry,
                                    Mobile = @Mobile,
                                    Email = @Email,
                                    Website = @Website,
                                    Question1 = @Question1,
                                    Question1Code = @Question1Code,
                                    Question2 = @Question2,
                                    Question2Code = @Question2Code,
                                    Question3 = @Question3,
                                    Question3Code = @Question3Code,
                                    Question4 = @Question4,
                                    Question4Code = @Question4Code,
                                    Question5 = @Question5 Where ID = @ID";

                    cmd.Parameters.Add(new SqlParameter("@Title", sTitle));
                    cmd.Parameters.Add(new SqlParameter("@FirstName", sFirstName));
                    cmd.Parameters.Add(new SqlParameter("@FamilyName", sFamilyName));
                    cmd.Parameters.Add(new SqlParameter("@Country", sCountry));
                    cmd.Parameters.Add(new SqlParameter("@Nationality", sNationality));
                    cmd.Parameters.Add(new SqlParameter("@City", sCity));
                    cmd.Parameters.Add(new SqlParameter("@MobileCountry", sMobileCountry));
                    cmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                    cmd.Parameters.Add(new SqlParameter("@ID", this.EditID));
                    //cmd.Parameters.Add(new SqlParameter("@DateAdded", DateTime.Now));
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    
                    Response.Redirect("registrations.aspx");
                }
            }
        }

        private void SendEmail(string visitorID, string FullName, string Email)
        {
            try
            {
                MailMessage mail = new MailMessage();
                string mailTo = Email;
                mail.To.Add(mailTo);
                mail.Subject = "HIS2019 Symposium - Registration Confirmation";
                mail.From = new MailAddress("his2019@mbrsc.ae", "Human In Space Symposium");
                mail.ReplyToList.Add("his2019@mbrsc.ae");
                mail.Bcc.Add("eventoconfirmemails@gmail.com");

                if (File.Exists(ConfigurationManager.AppSettings["FilePath1"].ToString() + "\\Invoice\\Invoice" + EditID+".pdf"))
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(ConfigurationManager.AppSettings["FilePath1"].ToString() + "\\Invoice\\Invoice" + EditID + ".pdf");
                    mail.Attachments.Add(attachment);
                }
                
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
                SmtpClient client = new SmtpClient("smtp.sendgrid.net", 587);
                client.Credentials = new NetworkCredential("mohsinxmushtaq", "mohsin@123");
                string strPath = Server.MapPath("~\\ConfirmationEmail.html");

                string strContent = System.IO.File.ReadAllText(strPath);
                strContent = strContent.Replace("{ID}", visitorID);
                strContent = strContent.Replace("{name}", FullName);
                mail.Body = strContent;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Registrations.cs", "SendEmail", ex, "HttpGet");
            }
        }

        void download(int RegID, string table, string fullName, string company, string country, string city, string phone)
        {
            try
            {
                string Invoicepath = "", invoiceDirectory = "", emailPath = "", emailDirectory = "", emailBody;

                Invoicepath = AppDomain.CurrentDomain.BaseDirectory + "\\SampleInvoice.html";
                invoiceDirectory = ConfigurationManager.AppSettings["FilePath1"].ToString() + "\\Invoice\\Invoice" + RegID + ".pdf";
                emailPath = AppDomain.CurrentDomain.BaseDirectory + "\\ConfirmationEmail.html";
                emailDirectory = ConfigurationManager.AppSettings["FilePath1"].ToString() + "\\Confirmation\\Confirmation" + RegID + ".pdf";

                //Invoice
                string Invoicebody = System.IO.File.ReadAllText(Invoicepath);
                Invoicebody = Invoicebody.Replace("{ID}", RegID.ToString());
                Invoicebody = Invoicebody.Replace("{Date}", DateTime.Now.ToString());
                Invoicebody = Invoicebody.Replace("{name}", fullName);
                Invoicebody = Invoicebody.Replace("{company}", company);
                Invoicebody = Invoicebody.Replace("{country}", country);
                Invoicebody = Invoicebody.Replace("{city}", city);
                Invoicebody = Invoicebody.Replace("{phone}", phone);
                Invoicebody = Invoicebody.Replace("{{TableWillComeHere}}", table);

                //Generating invoice in directory
                FileStream fileStream = new FileStream(invoiceDirectory, FileMode.Create, FileAccess.ReadWrite);
                HtmlConverter.ConvertToPdf(Invoicebody, fileStream);

                //Creating Email body
                emailBody = System.IO.File.ReadAllText(emailPath);
                emailBody = emailBody.Replace("{name}", fullName);
                emailBody = emailBody.Replace("{ID}", RegID.ToString());

                //generating email in directory
                fileStream = new FileStream(emailDirectory, FileMode.Create, FileAccess.ReadWrite);
                HtmlConverter.ConvertToPdf(emailBody, fileStream);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("RegistrationController.cs", "download", ex, "HttpPost");
            }
        }

        public void GenerateInvoice(string Amount, string TotalAmount, string IsAP, string APAmount, string TicketType, string fullName, string company, string country, string city, string phone)
        {
            try
            {
                string html = "";
                int amount = Convert.ToInt32(Amount), totalAmount = Convert.ToInt32(TotalAmount);
                double actual = Math.Round((Convert.ToDouble(Amount) / 105) * 100);
                double vat = Convert.ToDouble(Amount) - actual;

                html = @"<tr>
                        <td> 1 </td >
                        <td> " + TicketType + @" </td>
                        <td> 1 </td>
                        <td> EA </td>
                        <td> " + actual + @" </td>
                        <td> " + actual + @" </td>
                        <td> 5.00 </td>
                        <td> " + vat + @" </td>
                        <td> " + Amount + @" </td>
                    </tr>
                    ";

                if (IsAP == "Yes")
                {
                    actual = Math.Round((Convert.ToDouble(APAmount) / 105) * 100);
                    vat = Convert.ToDouble(APAmount) - actual;
                    html += @"<tr>
                        <td> 2 </td >
                        <td>Accompanying Persons</td>
                        <td> 1 </td>
                        <td> EA </td>
                        <td> " + actual + @" </td>
                        <td> " + actual + @" </td>
                        <td> 5.00 </td>
                        <td> " + vat + @" </td>
                        <td> " + APAmount + @" </td>
                    </tr>";
                }
                html += "<tr><td colspan = '8'> Total in USD </td><td> " + Convert.ToDouble(TotalAmount) + @" </td>
                     </tr>
                     <tr>
                         <td colspan = '9'> Amount in words(USD): " + NumberToWords(Convert.ToInt32(TotalAmount)) + @" only</td>
                      </tr> ";

                download(EditID, html, fullName, company, country, city, phone);

            }
            catch (Exception ex)
            {
                ErrorLogger.Log("RegistrationController.cs", "GenerateInvoice", ex, "HttpPost");
            }
        }
        public static string NumberToWords(int number)
        {
            if (number == 0)
                return "Zero";

            if (number < 0)
                return "Minus " + NumberToWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += NumberToWords(number / 1000000) + " Million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += NumberToWords(number / 1000) + " Thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += NumberToWords(number / 100) + " Hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "And ";

                var unitsMap = new[] { "Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
                var tensMap = new[] { "Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }

        
    }

}