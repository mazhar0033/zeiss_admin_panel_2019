﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class question : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Title = "Admin - Questioner";
            GetQuestion1();
            //GetQuestion2();
            //GetQuestion3();
            GetQuestion4();
            GetQuestion5();
        }

        private void GetQuestion1()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"exec GetQuestion1";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvQuestion1.DataSource = dSet.Tables["Table"].DefaultView;
                        gvQuestion1.DataBind();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        //private void GetQuestion2()
        //{
        //    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
        //    {
        //        conn.Open();
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.Connection = conn;
        //            cmd.CommandTimeout = 500;
        //            SqlDataAdapter da = null;
        //            DataSet dSet = new DataSet();
        //            try
        //            {
        //                string sql = @"exec GetQuestion2";
        //                da = new SqlDataAdapter(sql, conn);
        //                da.Fill(dSet, "Table");
        //                gvQuestion2.DataSource = dSet.Tables["Table"].DefaultView;
        //                gvQuestion2.DataBind();
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //            finally
        //            {
        //                dSet.Dispose();
        //                da.Dispose();
        //                conn.Close();
        //                conn.Dispose();
        //            }
        //        }
        //    }
        //}

        //private void GetQuestion3()
        //{
        //    using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
        //    {
        //        conn.Open();
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.Connection = conn;
        //            cmd.CommandTimeout = 500;
        //            SqlDataAdapter da = null;
        //            DataSet dSet = new DataSet();
        //            try
        //            {
        //                string sql = @"exec GetQuestion3";
        //                da = new SqlDataAdapter(sql, conn);
        //                da.Fill(dSet, "Table");
        //                gvQuestion3.DataSource = dSet.Tables["Table"].DefaultView;
        //                gvQuestion3.DataBind();
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //            }
        //            finally
        //            {
        //                dSet.Dispose();
        //                da.Dispose();
        //                conn.Close();
        //                conn.Dispose();
        //            }
        //        }
        //    }
        //}

        private void GetQuestion4()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;

                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"exec GetQuestion4";

                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvQuestion4.DataSource = dSet.Tables["Table"].DefaultView;
                        gvQuestion4.DataBind();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        private void GetQuestion5()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;

                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"exec GetQuestion5";

                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvQuestion5.DataSource = dSet.Tables["Table"].DefaultView;
                        gvQuestion5.DataBind();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

         
    }
}