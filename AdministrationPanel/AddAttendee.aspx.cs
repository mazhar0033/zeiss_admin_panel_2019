﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using System.Globalization;
using Newtonsoft.Json.Linq;
using OnlineRegistration.Logger;
using System.IO;
using System.Configuration;

namespace AdministrationPanel
{
    public partial class AddAttendee : System.Web.UI.Page
    {
        protected MemberInfo member = null;
        ICacheManager DTCMCache = CacheFactory.GetCacheManager("DTCM");
        public string DTCMBarcode;
        private const string popup = "No";
        static readonly char[] padding = { '=' };
        protected void Page_Load(object sender, EventArgs e)
        {
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string UserType = this.member.Type;
            if (UserType != "SuperAdmin")
            {
                btnRegister.Visible = false;
            }
            this.Page.Title = "Admin - Registrations";
            if (!IsPostBack)
            {
                PopulateCountry();
                UpdateStatus();
            }
        }

        public void PopulateCountry()
        {
            DataTable country = new DataTable();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT ID, CountryCode,Country,Code FROM Country", con);
                    adapter.Fill(country);

                    ddlNationality.DataSource = country;
                    ddlNationality.DataTextField = "Country";
                    ddlNationality.DataValueField = "ID";
                    ddlNationality.DataBind();

                    ddlMobileCountry.DataSource = country;
                    ddlMobileCountry.DataTextField = "Code";
                    ddlMobileCountry.DataValueField = "ID";
                    ddlMobileCountry.DataBind();

                    ddlCountry.DataSource = country;
                    ddlCountry.DataTextField = "Country";
                    ddlCountry.DataValueField = "ID";
                    ddlCountry.DataBind();



                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            ddlNationality.Items.Insert(0, new ListItem(string.Empty, ""));
            ddlMobileCountry.Items.Insert(0, new ListItem(string.Empty, ""));
            ddlCountry.Items.Insert(0, new ListItem(string.Empty, ""));


        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            string sTRNNumber = txtTRNNumber.Text;
            string sTitle = ddlTitle.SelectedValue;
            string sFirstName = txtFirstName.Text;
            string sLastName = txtLastName.Text;
            string sJobTitle = txtJobTitle.Text;
            string sCompany = txtCompany.Text;
            string sCountry = ddlCountry.SelectedItem.Text;
            string sCity = txtCity.Text;
            string sZip = string.Empty;
            string sEmail = txtEmail.Text;
            string sMobileCountryCode = ddlMobileCountry.SelectedItem.Text.Split('(', ')')[1];
            string sMobile = string.Format("{0} {1}", sMobileCountryCode, txtMobileNumber.Text);
            string sNationality = ddlNationality.SelectedItem.Text;
            string sBrowserType = string.Format("User Agent:{0}, Browser : {1}, Version  : {2}, Major  : {3}, Minor  : {4}", Request.UserAgent, Request.Browser.Browser, Request.Browser.Version, Request.Browser.MajorVersion, Request.Browser.MinorVersion);
            string sArea = string.Empty;
            string sDTCMPriceTypeCode = string.Empty;
            string sTicketType = rdotickettype.SelectedValue;
            string sVariousPanels = string.Empty;
            string sAge = "25-34";
            string sDOB = GetRandomdate("25-34");
            decimal iRegisterAmount = 0;
            decimal Total = 0;
            decimal TotalAmount = 0;
            decimal PerAmount = 0;
            string sLanguage = "English";
            string sAreyouamember = string.Empty;
            string sRegisterType = string.Empty;
            string sAccompanyingperson = "No";
            string sATitle = string.Empty;
            string sAFirstName = string.Empty;
            string sALastName = string.Empty;
            int iAccompanyingpersonPrice = 0;
            string sComments = txtComments.Text;
            if (sTicketType == "A1")
            {
                iRegisterAmount = (decimal)2697.45;
                sRegisterType = "Delegate";

            }
            else if (sTicketType == "A2")
            {
                iRegisterAmount = (decimal)3082.80;
                sRegisterType = "Delegate";

            }

            Total = iRegisterAmount;
            //PerAmount = (decimal)((5.5 * Total) / 100);
            TotalAmount = PerAmount + Total;

            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.Parameters.Clear();

                        cmd.CommandText = "SELECT COUNT(*) FROM Registrations WHERE Email=@Email and Status in (-2,-1)";
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));

                        bool isExisit = ((int)cmd.ExecuteScalar() > 0);
                        cmd.Parameters.Clear();

                        if (isExisit)
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = "Your previous transaction is still not completed. Please try after some time.";
                            return;
                        }

                        //DTCM API -
                        #region API
                        //var PerformancePrice = JObject.Parse(objDTCMAPI.GetPerformancePrice(Token));
                        //JToken jtoken = JObject.Parse(objDTCMAPI.GetPerformanceAvailability(Token));
                        //string PriceCategoriesPerformanceAvailability = JsonConvert.SerializeObject(AllChildren(jtoken).First(c => c.Type == JTokenType.Array && c.Path.Contains("PriceCategories")).Children<JObject>());
                        //var objPriceCat = JsonConvert.DeserializeObject<List<PriceCategory>>(PriceCategoriesPerformanceAvailability);
                        //var selectedPriceCategory = from element in objPriceCat where element.PriceCategoryId == Int32.Parse(sArea) select element;
                        //var s_PriceCat_ID = selectedPriceCategory.Select(o => o.Availability);
                        //var sPriceID = s_PriceCat_ID.FirstOrDefault();

                        //if (sPriceID.SoldOut)
                        //{
                        //    divError.Attributes.Add("style", "display:block");
                        //    litMessage.Text = "The Selected Tickets are sold out";
                        //    return;
                        //}

                        //JToken BasketPrices = JsonConvert.SerializeObject(AllChildren(PerformancePrice).First(c => c.Type == JTokenType.Array && c.Path.Contains("Prices")).Children<JObject>());
                        //var item = (JArray)JsonConvert.DeserializeObject(BasketPrices.ToString());
                        //// string PTypeCode = JsonConvert.SerializeObject(AllChildren(PerformancePrice).First(c => c.Type == JTokenType.Array && c.Path.Contains("PriceTypes")).Children<JObject>());
                        //List<Demand> demandList = new List<Demand>();
                        //Demand AdultDemand = new Demand() { priceTypeCode = sDTCMPriceTypeCode, quantity = 1, admits = 1, offerCode = "", qualifierCode = "", entitlement = "", customer = new Customer() };
                        //demandList.Add(AdultDemand);
                        //string json = JsonConvert.SerializeObject(demandList, Formatting.Indented);
                        //JToken GenerateBasket = JObject.Parse(objDTCMAPI.GenerateBasket(Token, json, sArea));
                        //string Basked = (string)GenerateBasket.SelectToken("Id");
                        //int SpousePrice = 0;
                        //if (chkAccPerson.Checked)
                        //{
                        //    var selectedSpousePriceCategory = from element in objPriceCat where element.PriceCategoryId == Int32.Parse("5") select element;
                        //    var Spouse_PriceCat_ID = selectedSpousePriceCategory.Select(o => o.Availability);
                        //    var SpousePriceID = Spouse_PriceCat_ID.FirstOrDefault();
                        //    if (SpousePriceID.SoldOut)
                        //    {
                        //        divError.Attributes.Add("style", "display:block");
                        //        litMessage.Text = "The Spouse Tickets are sold out";
                        //        return;
                        //    }
                        //    List<Demand> NewdemandList = new List<Demand>();
                        //    Demand NewAdultDemand = new Demand() { priceTypeCode = sDTCMPriceTypeCode, quantity = 1, admits = 1, offerCode = "", qualifierCode = "", entitlement = "", customer = new Customer() };
                        //    NewdemandList.Add(NewAdultDemand);
                        //    string Newjson = JsonConvert.SerializeObject(NewdemandList, Formatting.Indented);
                        //    JToken AddNewdataBasket = JObject.Parse(objDTCMAPI.AddToBasket(Token, Newjson, "5", Basked));
                        //    SpousePrice = (int)AddNewdataBasket.SelectToken("Offers[1].Demand[0].Prices[0].Net");
                        //}
                        //int Price = ((int)GenerateBasket.SelectToken("Offers[0].Demand[0].Prices[0].Net")) + (SpousePrice);
                        //#region CreateCustomer
                        //Customer cusDetail = new Customer() { lastname = sLastName, nationality = GetCodeCountry(sNationality), email = sEmail, dateofbirth = sDOB, internationalcode = sMobileCountryCode, areacode = txtMobileNumber.Text.Substring(0, 2), phonenumber = txtMobileNumber.Text.Substring(2) };
                        //if (!string.IsNullOrEmpty(sCity))
                        //    cusDetail.city = sCity;
                        //else
                        //    cusDetail.city = "city";
                        //if (!string.IsNullOrEmpty(sCity))
                        //    cusDetail.state = sCity;
                        //else
                        //    cusDetail.state = "state";
                        //cusDetail.countrycode = GetCodeCountry(sCountry);

                        //var serializer = new JavaScriptSerializer();
                        //string CoustomerDetail = serializer.Serialize(cusDetail);
                        //JToken Coustomer = JObject.Parse(objDTCMAPI.GenerateCoustomer(Token, CoustomerDetail));
                        //int Coustomerid = (Int32)Coustomer.SelectToken("ID");
                        //int CoustomerAccount = (Int32)Coustomer.SelectToken("Account");
                        //string CoustomerAFile = (string)Coustomer.SelectToken("AFile");
                        //#endregion

                        #endregion
                        //Insert Detials
                        cmd.Parameters.Clear();
                        cmd.CommandText = @"Insert into Registrations(TRNNumber,Title,FirstName,LastName,Position,Company,AgeGroup,Mobile,Email, City,Zip,Country,Nationality,RegisterAmount,Accompanyingperson,ATitle,AFirstName,ALastName,AAmount,Areyouamember, VariousPanels,DateRegister,RegisterType,PaymentGatewayFee,PromoCode,Amount,PaymentMode,Language,Comments,Ipaddress,BrowserType,Source,Status)
                                        Values(@TRNNumber,@Title,@FirstName,@LastName,@Position,@Company,@AgeGroup,@Mobile,@Email, @City,@Zip,@Country,@Nationality,@RegisterAmount,@Accompanyingperson,@ATitle,@AFirstName,@ALastName,@AAmount,@Areyouamember, @VariousPanels,@DateRegister,@RegisterType,@PaymentGatewayFee,@PromoCode,@Amount,@PaymentMode,@Language,@Comments,@Ipaddress,@BrowserType,@Source,@Status)" + "Select Scope_Identity()";

                        cmd.Parameters.Add(new SqlParameter("@TRNNumber", sTRNNumber));
                        cmd.Parameters.Add(new SqlParameter("@Title", sTitle));
                        cmd.Parameters.Add(new SqlParameter("@FirstName", sFirstName));
                        cmd.Parameters.Add(new SqlParameter("@LastName", sLastName));
                        cmd.Parameters.Add(new SqlParameter("@Position", sJobTitle));
                        cmd.Parameters.Add(new SqlParameter("@Company", sCompany));
                        cmd.Parameters.Add(new SqlParameter("@AgeGroup", sAge));
                        cmd.Parameters.Add(new SqlParameter("@Mobile", sMobile));
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                        cmd.Parameters.Add(new SqlParameter("@City", sCity));
                        cmd.Parameters.Add(new SqlParameter("@Zip", sZip));
                        cmd.Parameters.Add(new SqlParameter("@Country", sCountry));
                        cmd.Parameters.Add(new SqlParameter("@Nationality", sNationality));
                        cmd.Parameters.Add(new SqlParameter("@RegisterAmount", iRegisterAmount));
                        cmd.Parameters.Add(new SqlParameter("@Accompanyingperson", sAccompanyingperson));
                        cmd.Parameters.Add(new SqlParameter("@ATitle", sATitle));
                        cmd.Parameters.Add(new SqlParameter("@AFirstName", sAFirstName));
                        cmd.Parameters.Add(new SqlParameter("@ALastName", sALastName));
                        cmd.Parameters.Add(new SqlParameter("@AAmount", iAccompanyingpersonPrice));
                        cmd.Parameters.Add(new SqlParameter("@Areyouamember", sAreyouamember));
                        cmd.Parameters.Add(new SqlParameter("@VariousPanels", sVariousPanels));
                        cmd.Parameters.Add(new SqlParameter("@DateRegister", DateTime.Now));
                        cmd.Parameters.Add(new SqlParameter("@RegisterType", sRegisterType));
                        cmd.Parameters.Add(new SqlParameter("@PaymentGatewayFee", PerAmount));
                        cmd.Parameters.Add(new SqlParameter("@PromoCode", ""));
                        cmd.Parameters.Add(new SqlParameter("@Amount", TotalAmount));
                        cmd.Parameters.Add(new SqlParameter("@PaymentMode", "Bank"));
                        cmd.Parameters.Add(new SqlParameter("@Language", sLanguage));
                        cmd.Parameters.Add(new SqlParameter("@Comments", sComments));
                        cmd.Parameters.Add(new SqlParameter("@Ipaddress", GetIp()));
                        cmd.Parameters.Add(new SqlParameter("@BrowserType", sBrowserType));
                        cmd.Parameters.Add(new SqlParameter("@Source", "Admin Panel"));
                        cmd.Parameters.Add(new SqlParameter("@Status", "-6"));
                        int status = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        cmd.Parameters.Clear();

                        #region BasketDetails
                        //cmd.CommandText = "Insert into BasketDetails(BasketID,PriceNet,Customerid,CustomerAccount,CustomerAFile,MerchantRef) Values(@BasketID,@PriceNet,@Customerid,@CustomerAccount,@CustomerAFile,@MerchantRef)";
                        //cmd.Parameters.Add(new SqlParameter("@BasketID", Basked));
                        //cmd.Parameters.Add(new SqlParameter("@PriceNet", Price));
                        //cmd.Parameters.Add(new SqlParameter("@Customerid", Coustomerid));
                        //cmd.Parameters.Add(new SqlParameter("@CustomerAccount", CoustomerAccount));
                        //cmd.Parameters.Add(new SqlParameter("@CustomerAFile", CoustomerAFile));
                        //cmd.Parameters.Add(new SqlParameter("@MerchantRef", status));
                        //cmd.ExecuteNonQuery();
                        //cmd.Parameters.Clear();
                        #endregion

                        if (status > 0)
                        {
                            SendConfirmation(status.ToString());
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Data has been submitted successfully'); window.location='" + Request.ApplicationPath + "/registrations.aspx';", true);

                        }
                        else
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = "status is null.";
                            return;
                        }
                    }


                }
                catch (Exception ex)
                {
                    litMessage.Text = ex.ToString();
                    ErrorLogger.Log("AddAttendee.cs", "btnRegister_Click", ex, "HttpPost");
                }
            }

        }

        public string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }

        private void SendConfirmation(string RegID)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandTimeout = 500;
                    cmd.Connection = conn;
                    cmd.CommandText = @"SELECT  *  FROM Registrations   WHERE (RegID = @RegID)";
                    cmd.Parameters.Add(new SqlParameter("@RegID", RegID));
                    SqlDataReader reader = cmd.ExecuteReader();
                    cmd.Parameters.Clear();
                    if (reader.Read())
                    {
                        try
                        {
                            string RowForReg = string.Empty;
                            int sRegID = reader.GetInt32(reader.GetOrdinal("RegID"));
                            string sEmail = reader.GetString(reader.GetOrdinal("Email"));
                            string sCompany = reader.GetString(reader.GetOrdinal("Company"));
                            string sFirstName = reader.GetString(reader.GetOrdinal("FirstName"));
                            string sLastName = reader.GetString(reader.GetOrdinal("LastName"));
                            string sCity = reader.GetString(reader.GetOrdinal("City"));
                            string sCountry = reader.GetString(reader.GetOrdinal("Country"));
                            string sMobile = reader.GetString(reader.GetOrdinal("Mobile"));
                            string sRegisterType = reader.GetString(reader.GetOrdinal("RegisterType"));
                            string sAmount = reader.GetString(reader.GetOrdinal("Amount"));
                            string TRNNumber = reader.GetString(reader.GetOrdinal("TRNNumber"));
                            string sDateRegister = reader.GetDateTime(reader.GetOrdinal("DateRegister")).ToString("dd-MMM-yyyy");
                            string Reference = "DDC2019";
                            string Description = "Dubai Diamond Conference 2019";
                            string strPath = Server.MapPath("Invoice.html");
                            double sPerAmount = 0.00;
                            double sUnitPrice = 0.00;
                            sPerAmount = ((double)double.Parse(sAmount) / 105) * 5;
                            sUnitPrice = ((double)double.Parse(sAmount) - sPerAmount);
                            string strContent = System.IO.File.ReadAllText(strPath);

                            var varsAmount = Convert.ToDecimal(sAmount);
                            float floatAmount = (float)varsAmount;

                            var varsPerAmount = Convert.ToDecimal(sPerAmount);
                            float floatsPerAmount = (float)varsPerAmount;

                            var varsUnitPrice = Convert.ToDecimal(sUnitPrice);
                            float floatsUnitPrice = (float)varsUnitPrice;

                            strContent = strContent.Replace("{ID}", RegID.ToString());
                            strContent = strContent.Replace("{Reference}", Reference);
                            strContent = strContent.Replace("{Description}", Description);
                            strContent = strContent.Replace("{DATE}", sDateRegister);
                            strContent = strContent.Replace("{FirstName}", textInfo.ToTitleCase(sFirstName));
                            strContent = strContent.Replace("{LastName}", textInfo.ToTitleCase(sLastName));
                            strContent = strContent.Replace("{Company}", textInfo.ToTitleCase(sCompany));
                            strContent = strContent.Replace("{Country}", textInfo.ToTitleCase(sCountry));
                            strContent = strContent.Replace("{City}", textInfo.ToTitleCase(sCity));
                            strContent = strContent.Replace("{Mobile}", sMobile);
                            strContent = strContent.Replace("{RegisterType}", sRegisterType);
                            strContent = strContent.Replace("{TOTALAmount}", floatAmount.ToString("N2"));
                            strContent = strContent.Replace("{VATAmount}", floatsPerAmount.ToString("N2"));
                            strContent = strContent.Replace("{UnitPrice}", floatsUnitPrice.ToString("N2"));
                            strContent = strContent.Replace("{Amount}", Math.Round(sUnitPrice).ToString());
                            strContent = strContent.Replace("{TRNNumber}", TRNNumber);
                            strContent = strContent.Replace("{TOTALAmountWords}", DecimalToWordExtension.ToWords(decimal.Parse(sAmount)));


                            StringReader sr = new StringReader(strContent);

                            iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 0f);
                            iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
                            using (MemoryStream memoryStream = new MemoryStream())
                            {
                                iTextSharp.text.pdf.PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, memoryStream);
                                pdfDoc.Open();

                                htmlparser.Parse(sr);
                                pdfDoc.Close();

                                byte[] bytes = memoryStream.ToArray();
                                memoryStream.Close();
                                FileStream fs = new FileStream(ConfigurationManager.AppSettings["FilePath"].ToString() + sRegID.ToString() + ".pdf", FileMode.OpenOrCreate);
                                fs.Write(bytes, 0, bytes.Length);
                                fs.Close();
                            }

                            SendEmail(RegID.ToString(), sFirstName, sLastName, sEmail, sCompany);

                        }
                        catch (Exception ex)
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = ex.Message.ToString();
                            ErrorLogger.Log("AddAttendee.cs", "SendConfirmation", ex, "HttpPost");
                        }
                        finally
                        {

                        }
                        reader.Close();
                    }

                }
            }
        }

        private void SendEmail(string visitorID, string FirstName, string LastName, string Email, string sCompany)
        {
            try
            {
                string sNAME = FirstName + " " + LastName;
                sNAME = sNAME.Trim();
                MailMessage mail = new MailMessage();
                string mailTo = Email;
                mail.To.Add(mailTo);
                mail.Subject = "Thank you for registering Dubai Diamond Conference 2019";
                mail.From = new MailAddress("events@dmcc.ae", "DDC 2019");
                mail.ReplyTo = new MailAddress("events@dmcc.ae");
                mail.Bcc.Add("eventoconfirmemails@gmail.com");
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
                string file = visitorID.ToString() + ".pdf";
                if (!string.IsNullOrEmpty(file) && File.Exists(ConfigurationManager.AppSettings["FilePath"].ToString() + (file)))
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(ConfigurationManager.AppSettings["FilePath"].ToString() + visitorID.ToString() + ".pdf");
                    mail.Attachments.Add(attachment);
                }
                SmtpClient client = new SmtpClient("smtp.sendgrid.net", 587);
                client.Credentials = new NetworkCredential("mohsinxmushtaq", "mohsin@123");
                string strPath = Server.MapPath("Conference.html");
                string strContent = System.IO.File.ReadAllText(strPath);
                strContent = strContent.Replace("{ID}", visitorID);
                strContent = strContent.Replace("{DATE}", DateTime.Now.ToString("dd/MM/yyyy"));
                strContent = strContent.Replace("{FIRSTNAME}", FirstName);
                strContent = strContent.Replace("{LASTNAME}", LastName);
                strContent = strContent.Replace("{Company}", sCompany);
                mail.Body = strContent;
                client.Send(mail);
            }
            catch (Exception ex)
            {

                divError.Attributes.Add("style", "display:block");
                litMessage.Text = ex.Message.ToString();
                ErrorLogger.Log("AddAttendee.cs", "SendEmail", ex, "HttpPost");
            }
        }

        protected void btnRegisterWithCard_Click(object sender, EventArgs e)
        {
            //DTCMAPI objDTCMAPI = new DTCMAPI();
            SalesForce objSalesForce = new SalesForce();
            string Token = string.Empty;
            string sTRNNumber = txtTRNNumber.Text;
            string sTitle = ddlTitle.SelectedValue;
            string sFirstName = txtFirstName.Text;
            string sLastName = txtLastName.Text;
            string sJobTitle = txtJobTitle.Text;
            string sCompany = txtCompany.Text;
            string sCountry = ddlCountry.SelectedItem.Text;
            string sCity = txtCity.Text;
            string sZip = string.Empty;
            string sEmail = txtEmail.Text;
            string sMobileCountryCode = ddlMobileCountry.SelectedItem.Text.Split('(', ')')[1];
            string sMobile = string.Format("{0} {1}", sMobileCountryCode, txtMobileNumber.Text);
            string sNationality = ddlNationality.SelectedItem.Text;
            string sBrowserType = string.Format("User Agent:{0}, Browser : {1}, Version  : {2}, Major  : {3}, Minor  : {4}", Request.UserAgent, Request.Browser.Browser, Request.Browser.Version, Request.Browser.MajorVersion, Request.Browser.MinorVersion);
            string sArea = string.Empty;
            string sDTCMPriceTypeCode = string.Empty;
            string sTicketType = rdotickettype.SelectedValue;
            string sVariousPanels = string.Empty;
            string sAge = "25-34";
            string sDOB = GetRandomdate("25-34");
            decimal iRegisterAmount = 0;
            decimal Total = 0;
            decimal TotalAmount = 0;
            decimal PerAmount = 0;
            string sLanguage = "English";
            string sAreyouamember = string.Empty;
            string sRegisterType = string.Empty;
            string sAccompanyingperson = "No";
            string sATitle = string.Empty;
            string sAFirstName = string.Empty;
            string sALastName = string.Empty;
            int iAccompanyingpersonPrice = 0;
            string sComments = txtComments.Text;
            if (sTicketType == "A1")
            {
                iRegisterAmount = (decimal)2697.45;
                sRegisterType = "Delegate";

            }
            else if (sTicketType == "A2")
            {
                iRegisterAmount = (decimal)3082.80;
                sRegisterType = "Delegate";

            }


            Total = iRegisterAmount;
            //PerAmount = (decimal)((5.5 * Total) / 100);
            TotalAmount = PerAmount + Total;

            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.Parameters.Clear();

                        cmd.CommandText = "SELECT COUNT(*) FROM Registrations WHERE Email=@Email and Status in (-2,-1)";
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));

                        bool isExisit = ((int)cmd.ExecuteScalar() > 0);
                        cmd.Parameters.Clear();

                        if (isExisit)
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = "Your previous transaction is still not completed. Please try after some time.";
                            return;
                        }

                        //#region DTCM
                        //if (isSessionExists("Token"))
                        //{
                        //    Token = DTCMCache["Token"].ToString();
                        //}
                        //else
                        //{
                        //    //Token = "b6343694f35549d998c53577c783d145";
                        //    //SlidingTime TPSlidingTime = new SlidingTime(TimeSpan.FromSeconds((28800) - 500));
                        //    JToken jtokenTime = JObject.Parse(objDTCMAPI.Oauth());
                        //    Token = (string)jtokenTime.SelectToken("access_token");
                        //    AbsoluteTime TPSlidingTime = new AbsoluteTime(DateTime.Now.AddSeconds((int)jtokenTime.SelectToken("expires_in") - 500));
                        //    DTCMCache.Add("Token", Token, Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemPriority.NotRemovable, new CacheLogPolicy(), TPSlidingTime);
                        //    using (SqlConnection connn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
                        //    {
                        //        try
                        //        {
                        //            connn.Open();
                        //            using (SqlCommand cmds = new SqlCommand())
                        //            {
                        //                //#region TokenDetails
                        //                cmds.Connection = connn;
                        //                cmds.CommandTimeout = 500;
                        //                cmds.Parameters.Clear();
                        //                cmds.CommandText = "Insert into DTCMTokens(Token,DateTime,Status) Values(@Token,@DateTime,@Status)";
                        //                cmds.Parameters.Add(new SqlParameter("@Token", Token));
                        //                cmds.Parameters.Add(new SqlParameter("@DateTime", DateTime.Now));
                        //                cmds.Parameters.Add(new SqlParameter("@Status", "1"));
                        //                cmds.ExecuteNonQuery();
                        //                cmds.Parameters.Clear();
                        //                //#endregion
                        //            }
                        //        }
                        //        catch (Exception ex)
                        //        {
                        //            litMessage.Text = ex.ToString();
                        //            ErrorLogger.Log("AddAttendee.cs", "btnRegisterWithCard_Click", ex, "HttpGetToken");
                        //        }
                        //    }
                        //}
                        //#endregion

                        ////DTCM API -
                        //#region API
                        //var PerformancePrice = JObject.Parse(objDTCMAPI.GetPerformancePrice(Token));
                        //JToken jtoken = JObject.Parse(objDTCMAPI.GetPerformanceAvailability(Token));
                        //string PriceCategoriesPerformanceAvailability = JsonConvert.SerializeObject(AllChildren(jtoken).First(c => c.Type == JTokenType.Array && c.Path.Contains("PriceCategories")).Children<JObject>());
                        //var objPriceCat = JsonConvert.DeserializeObject<List<PriceCategory>>(PriceCategoriesPerformanceAvailability);
                        //var selectedPriceCategory = from element in objPriceCat where element.PriceCategoryId == Int32.Parse(sArea) select element;
                        //var s_PriceCat_ID = selectedPriceCategory.Select(o => o.Availability);
                        //var sPriceID = s_PriceCat_ID.FirstOrDefault();

                        //if (sPriceID.SoldOut)
                        //{
                        //    divError.Attributes.Add("style", "display:block");
                        //    litMessage.Text = "The Selected Tickets are sold out";
                        //    return;
                        //}

                        //JToken BasketPrices = JsonConvert.SerializeObject(AllChildren(PerformancePrice).First(c => c.Type == JTokenType.Array && c.Path.Contains("Prices")).Children<JObject>());
                        //var item = (JArray)JsonConvert.DeserializeObject(BasketPrices.ToString());
                        //// string PTypeCode = JsonConvert.SerializeObject(AllChildren(PerformancePrice).First(c => c.Type == JTokenType.Array && c.Path.Contains("PriceTypes")).Children<JObject>());
                        //List<Demand> demandList = new List<Demand>();
                        //Demand AdultDemand = new Demand() { priceTypeCode = sDTCMPriceTypeCode, quantity = 1, admits = 1, offerCode = "", qualifierCode = "", entitlement = "", customer = new Customer() };
                        //demandList.Add(AdultDemand);
                        //string json = JsonConvert.SerializeObject(demandList, Formatting.Indented);
                        //JToken GenerateBasket = JObject.Parse(objDTCMAPI.GenerateBasket(Token, json, sArea));
                        //string Basked = (string)GenerateBasket.SelectToken("Id"); 
                        //int Price = ((int)GenerateBasket.SelectToken("Offers[0].Demand[0].Prices[0].Net"));

                        //#region CreateCustomer
                        //Customer cusDetail = new Customer() { lastname = sLastName, nationality = GetCodeCountry(sNationality), email = sEmail, dateofbirth = sDOB, internationalcode = sMobileCountryCode, areacode = txtMobileNumber.Text.Substring(0, 2), phonenumber = txtMobileNumber.Text.Substring(2) };
                        //if (!string.IsNullOrEmpty(sCity))
                        //    cusDetail.city = sCity;
                        //else
                        //    cusDetail.city = "city";
                        //if (!string.IsNullOrEmpty(sCity))
                        //    cusDetail.state = sCity;
                        //else
                        //    cusDetail.state = "state";
                        //cusDetail.countrycode = GetCodeCountry(sCountry);

                        //var serializer = new JavaScriptSerializer();
                        //string CoustomerDetail = serializer.Serialize(cusDetail);
                        //JToken Coustomer = JObject.Parse(objDTCMAPI.GenerateCoustomer(Token, CoustomerDetail));
                        //int Coustomerid = (Int32)Coustomer.SelectToken("ID");
                        //int CoustomerAccount = (Int32)Coustomer.SelectToken("Account");
                        //string CoustomerAFile = (string)Coustomer.SelectToken("AFile");
                        //#endregion
                        //#endregion

                        //Insert Detials
                        cmd.Parameters.Clear();
                        cmd.CommandText = @"Insert into Registrations(TRNNumber,Title,FirstName,LastName,Position,Company,AgeGroup,Mobile,Email, City,Zip,Country,Nationality,RegisterAmount,Accompanyingperson,ATitle,AFirstName,ALastName,AAmount,Areyouamember, VariousPanels,DateRegister,RegisterType,PaymentGatewayFee,PromoCode,Amount,PaymentMode,Language,Comments,Ipaddress,BrowserType,Source,Status)
                                        Values(@TRNNumber,@Title,@FirstName,@LastName,@Position,@Company,@AgeGroup,@Mobile,@Email, @City,@Zip,@Country,@Nationality,@RegisterAmount,@Accompanyingperson,@ATitle,@AFirstName,@ALastName,@AAmount,@Areyouamember, @VariousPanels,@DateRegister,@RegisterType,@PaymentGatewayFee,@PromoCode,@Amount,@PaymentMode,@Language,@Comments,@Ipaddress,@BrowserType,@Source,@Status)" + "Select Scope_Identity()";

                        cmd.Parameters.Add(new SqlParameter("@TRNNumber", sTRNNumber));
                        cmd.Parameters.Add(new SqlParameter("@Title", sTitle));
                        cmd.Parameters.Add(new SqlParameter("@FirstName", sFirstName));
                        cmd.Parameters.Add(new SqlParameter("@LastName", sLastName));
                        cmd.Parameters.Add(new SqlParameter("@Position", sJobTitle));
                        cmd.Parameters.Add(new SqlParameter("@Company", sCompany));
                        cmd.Parameters.Add(new SqlParameter("@AgeGroup", sAge));
                        cmd.Parameters.Add(new SqlParameter("@Mobile", sMobile));
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                        cmd.Parameters.Add(new SqlParameter("@City", sCity));
                        cmd.Parameters.Add(new SqlParameter("@Zip", sZip));
                        cmd.Parameters.Add(new SqlParameter("@Country", sCountry));
                        cmd.Parameters.Add(new SqlParameter("@Nationality", sNationality));
                        cmd.Parameters.Add(new SqlParameter("@RegisterAmount", iRegisterAmount));
                        cmd.Parameters.Add(new SqlParameter("@Accompanyingperson", sAccompanyingperson));
                        cmd.Parameters.Add(new SqlParameter("@ATitle", sATitle));
                        cmd.Parameters.Add(new SqlParameter("@AFirstName", sAFirstName));
                        cmd.Parameters.Add(new SqlParameter("@ALastName", sALastName));
                        cmd.Parameters.Add(new SqlParameter("@AAmount", iAccompanyingpersonPrice));
                        cmd.Parameters.Add(new SqlParameter("@Areyouamember", sAreyouamember));
                        cmd.Parameters.Add(new SqlParameter("@VariousPanels", sVariousPanels));
                        cmd.Parameters.Add(new SqlParameter("@DateRegister", DateTime.Now));
                        cmd.Parameters.Add(new SqlParameter("@RegisterType", sRegisterType));
                        cmd.Parameters.Add(new SqlParameter("@PaymentGatewayFee", PerAmount));
                        cmd.Parameters.Add(new SqlParameter("@PromoCode", ""));
                        cmd.Parameters.Add(new SqlParameter("@Amount", TotalAmount));
                        cmd.Parameters.Add(new SqlParameter("@PaymentMode", "Card"));
                        cmd.Parameters.Add(new SqlParameter("@Language", sLanguage));
                        cmd.Parameters.Add(new SqlParameter("@Comments", sComments));
                        cmd.Parameters.Add(new SqlParameter("@Ipaddress", GetIp()));
                        cmd.Parameters.Add(new SqlParameter("@BrowserType", sBrowserType));
                        cmd.Parameters.Add(new SqlParameter("@Source", "Admin Panel"));
                        cmd.Parameters.Add(new SqlParameter("@Status", "-2"));
                        int status = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        cmd.Parameters.Clear();

                        //#region BasketDetails
                        //cmd.CommandText = "Insert into BasketDetails(BasketID,PriceNet,Customerid,CustomerAccount,CustomerAFile,MerchantRef) Values(@BasketID,@PriceNet,@Customerid,@CustomerAccount,@CustomerAFile,@MerchantRef)";
                        //cmd.Parameters.Add(new SqlParameter("@BasketID", Basked));
                        //cmd.Parameters.Add(new SqlParameter("@PriceNet", Price));
                        //cmd.Parameters.Add(new SqlParameter("@Customerid", Coustomerid));
                        //cmd.Parameters.Add(new SqlParameter("@CustomerAccount", CoustomerAccount));
                        //cmd.Parameters.Add(new SqlParameter("@CustomerAFile", CoustomerAFile));
                        //cmd.Parameters.Add(new SqlParameter("@MerchantRef", status));
                        //cmd.ExecuteNonQuery();
                        //cmd.Parameters.Clear();
                        //#endregion

                        if (status > 0)
                        {
                            var GetOAuthToken = objSalesForce.GetOAuthToken();
                            if (GetOAuthToken == null)
                            {
                                litMessage.Text = "Failed to get Auth Token";
                                return;
                            }
                            int dbstatus = InsertOAuthToken(status, GetOAuthToken.access_token, GetOAuthToken.instance_url, GetOAuthToken.id, GetOAuthToken.token_type, GetOAuthToken.issued_at, GetOAuthToken.signature);
                            if (dbstatus > 0)
                            {
                                JToken GenerateProlianceServices = JObject.Parse(objSalesForce.DMCC_ProlianceServices_API(GetOAuthToken.access_token, status.ToString(), TotalAmount.ToString(), sFirstName, sEmail, "test", "DPMC"));
                                UpdateDMCCProlianceServices(status, (string)GenerateProlianceServices.SelectToken("TransactionReferenceNumber"), (string)GenerateProlianceServices.SelectToken("TransactionId"), (string)GenerateProlianceServices.SelectToken("Status"), (string)GenerateProlianceServices.SelectToken("ErrorMessage"), (string)GenerateProlianceServices.SelectToken("ErrorCode"), (string)GenerateProlianceServices.SelectToken("AccountNumber"), (string)GenerateProlianceServices.SelectToken("AccountName"));
                                string URL = "https://dmcc.secure.force.com/websitepayment?rid={0}&TransactionId={1}&servicetype=500";
                                URL = string.Format(URL, (string)GenerateProlianceServices.SelectToken("TransactionReferenceNumber"), (string)GenerateProlianceServices.SelectToken("TransactionId"));
                                Response.Redirect(URL, false);

                            }
                        }
                        else
                        {
                            divError.Attributes.Add("style", "display:block");
                            litMessage.Text = "status is null.";
                            return;
                        }
                    }


                }
                catch (Exception ex)
                {
                    divError.Attributes.Add("style", "display:block");
                    litMessage.Text = ex.Message.ToString();
                    ErrorLogger.Log("AddAttendee.cs", "btnRegisterWithCard_Click", ex, "HttpPost");
                }
            }
        }

        #region DTCMRelated
        class CacheLogPolicy : ICacheItemRefreshAction
        {
            //Implement Referesh Method of interface ICacheItemRefreshAction
            public void Refresh(string removedKey, object expiredValue, Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemRemovedReason removalReason)
            {

            }

        }

        private bool isSessionExists(string ChName)
        {
            if (DTCMCache[ChName] != null)
                return true;
            else
                return false;
        }

        private static IEnumerable<JToken> AllChildren(JToken json)
        {
            foreach (var c in json.Children())
            {
                yield return c;
                foreach (var cc in AllChildren(c))
                {
                    yield return cc;
                }
            }
        }

        private string GetCodeCountry(string sCountry)
        {
            KeyValuePair<string, string> kvpCountry = new KeyValuePair<string, string>();

            #region CountryCodes
            var data = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("Afghanistan","AF"),
            new KeyValuePair<string, string>("Aland Islands","AX"),
            new KeyValuePair<string, string>("Albania","AL"),
            new KeyValuePair<string, string>("Algeria","DZ"),
            new KeyValuePair<string, string>("American Samoa","AS"),
            new KeyValuePair<string, string>("Andorra","AD"),
            new KeyValuePair<string, string>("Angola","AO"),
            new KeyValuePair<string, string>("Anguilla","AI"),
            new KeyValuePair<string, string>("Antarctica","AQ"),
            new KeyValuePair<string, string>("Antigua and Barbuda","AG"),
            new KeyValuePair<string, string>("Argentina","AR"),
            new KeyValuePair<string, string>("Armenia","AM"),
            new KeyValuePair<string, string>("Aruba","AW"),
            new KeyValuePair<string, string>("Australia","AU"),
            new KeyValuePair<string, string>("Austria","AT"),
            new KeyValuePair<string, string>("Azerbaijan","AZ"),
            new KeyValuePair<string, string>("Bahamas","BS"),
            new KeyValuePair<string, string>("Bahrain","BH"),
            new KeyValuePair<string, string>("Bangladesh","BD"),
            new KeyValuePair<string, string>("Barbados","BB"),
            new KeyValuePair<string, string>("Belarus","BY"),
            new KeyValuePair<string, string>("Belgium","BE"),
            new KeyValuePair<string, string>("Belize","BZ"),
            new KeyValuePair<string, string>("Benin","BJ"),
            new KeyValuePair<string, string>("Bermuda","BM"),
            new KeyValuePair<string, string>("Bhutan","BT"),
            new KeyValuePair<string, string>("Bolivia","BO"),
            new KeyValuePair<string, string>("Bosnia","BA"),
            new KeyValuePair<string, string>("Botswana","BW"),
            new KeyValuePair<string, string>("Bouvet Island","BV"),
            new KeyValuePair<string, string>("Brazil","BR"),
            new KeyValuePair<string, string>("British Indian Ocean Territory","IO"),
            new KeyValuePair<string, string>("Brunei Darussalam","BN"),
            new KeyValuePair<string, string>("Bulgaria","BG"),
            new KeyValuePair<string, string>("Burkina Faso","BF"),
            new KeyValuePair<string, string>("Burundi","BI"),
            new KeyValuePair<string, string>("Cambodia","KH"),
            new KeyValuePair<string, string>("Cameroon","CM"),
            new KeyValuePair<string, string>("Canada","CA"),
            new KeyValuePair<string, string>("Cape Verde","CV"),
            new KeyValuePair<string, string>("Cayman Islands","KY"),
            new KeyValuePair<string, string>("Central African Republic","CF"),
            new KeyValuePair<string, string>("Chad","TD"),
            new KeyValuePair<string, string>("Chile","CL"),
            new KeyValuePair<string, string>("China","CN"),
            new KeyValuePair<string, string>("Christmas Island","CX"),
            new KeyValuePair<string, string>("Cocos (Keeling) Islands","CC"),
            new KeyValuePair<string, string>("Colombia","CO"),
            new KeyValuePair<string, string>("Comoros","KM"),
            new KeyValuePair<string, string>("Congo","CG"),
            new KeyValuePair<string, string>("Congo (The Democratic Republic of)","CD"),
            new KeyValuePair<string, string>("Cook Islands","CK"),
            new KeyValuePair<string, string>("Costa Rica","CR"),
            new KeyValuePair<string, string>("Côte d'Ivoire","CI"),
            new KeyValuePair<string, string>("Croatia","HR"),
            new KeyValuePair<string, string>("Cuba","CU"),
            new KeyValuePair<string, string>("Cyprus","CY"),
            new KeyValuePair<string, string>("Czech Republic","CZ"),
            new KeyValuePair<string, string>("Denmark","DK"),
            new KeyValuePair<string, string>("Djibouti","DJ"),
            new KeyValuePair<string, string>("Dominica","DM"),
            new KeyValuePair<string, string>("Dominican Republic","DO"),
            new KeyValuePair<string, string>("Ecuador","EC"),
            new KeyValuePair<string, string>("Egypt","EG"),
            new KeyValuePair<string, string>("El Salvador","SV"),
            new KeyValuePair<string, string>("Equatorial Guinea","GQ"),
            new KeyValuePair<string, string>("Eritrea","ER"),
            new KeyValuePair<string, string>("Estonia","EE"),
            new KeyValuePair<string, string>("Ethiopia","ET"),
            new KeyValuePair<string, string>("Falkland Islands (Malvinas)","FK"),
            new KeyValuePair<string, string>("Faroe Islands","FO"),
            new KeyValuePair<string, string>("Fiji","FJ"),
            new KeyValuePair<string, string>("Finland","FI"),
            new KeyValuePair<string, string>("France","FR"),
            new KeyValuePair<string, string>("French Guiana","GF"),
            new KeyValuePair<string, string>("French Polynesia","PF"),
            new KeyValuePair<string, string>("French Southern Territories","TF"),
            new KeyValuePair<string, string>("Gabon","GA"),
            new KeyValuePair<string, string>("Gambia","GM"),
            new KeyValuePair<string, string>("Georgia","GE"),
            new KeyValuePair<string, string>("Germany","DE"),
            new KeyValuePair<string, string>("Ghana","GH"),
            new KeyValuePair<string, string>("Gibraltar","GI"),
            new KeyValuePair<string, string>("Greece","GR"),
            new KeyValuePair<string, string>("Greenland","GL"),
            new KeyValuePair<string, string>("Grenada","GD"),
            new KeyValuePair<string, string>("Guadeloupe","GP"),
            new KeyValuePair<string, string>("Guam","GU"),
            new KeyValuePair<string, string>("Guatemala","GT"),
            new KeyValuePair<string, string>("Guernsey","GG"),
            new KeyValuePair<string, string>("Guinea","GN"),
            new KeyValuePair<string, string>("Guinea-Bissau","GW"),
            new KeyValuePair<string, string>("Guyana","GY"),
            new KeyValuePair<string, string>("Haiti","HT"),
            new KeyValuePair<string, string>("Heard Island and McDonald Islands","HM"),
            new KeyValuePair<string, string>("Holy See (Vatican City State)","VA"),
            new KeyValuePair<string, string>("Honduras","HN"),
            new KeyValuePair<string, string>("Hong Kong","HK"),
            new KeyValuePair<string, string>("Hungary","HU"),
            new KeyValuePair<string, string>("Iceland","IS"),
            new KeyValuePair<string, string>("India","IN"),
            new KeyValuePair<string, string>("Indonesia","ID"),
            new KeyValuePair<string, string>("Iran","IR"),
            new KeyValuePair<string, string>("Iraq","IQ"),
            new KeyValuePair<string, string>("Ireland","IE"),
            new KeyValuePair<string, string>("Isle of Man","IM"),
            new KeyValuePair<string, string>("Israel","IL"),
            new KeyValuePair<string, string>("Italy","IT"),
            new KeyValuePair<string, string>("Jamaica","JM"),
            new KeyValuePair<string, string>("Japan","JP"),
            new KeyValuePair<string, string>("Jersey","JE"),
            new KeyValuePair<string, string>("Jordan","JO"),
            new KeyValuePair<string, string>("Kazakhstan","KZ"),
            new KeyValuePair<string, string>("Kenya","KE"),
            new KeyValuePair<string, string>("Kiribati","KI"),
            new KeyValuePair<string, string>("Korea, Republic of","KR"),
            new KeyValuePair<string, string>("Korea (DPR)","KP"),
            new KeyValuePair<string, string>("Kuwait","KW"),
            new KeyValuePair<string, string>("Kyrgystan","KG"),
            new KeyValuePair<string, string>("Lao Peoples Democratic Republic","LA"),
            new KeyValuePair<string, string>("Latvia","LV"),
            new KeyValuePair<string, string>("Lebanon","LB"),
            new KeyValuePair<string, string>("Lesotho","LS"),
            new KeyValuePair<string, string>("Liberia","LR"),
            new KeyValuePair<string, string>("Libyan Arab Jamahiriya","LY"),
            new KeyValuePair<string, string>("Liechtenstein","LI"),
            new KeyValuePair<string, string>("Lithuania","LT"),
            new KeyValuePair<string, string>("Luxembourg","LU"),
            new KeyValuePair<string, string>("Macao","MO"),
            new KeyValuePair<string, string>("Macedonia (The Former Yugoslav Republic of)","MK"),
            new KeyValuePair<string, string>("Madagascar","MG"),
            new KeyValuePair<string, string>("Malawi","MW"),
            new KeyValuePair<string, string>("Malaysia","MY"),
            new KeyValuePair<string, string>("Maldives","MV"),
            new KeyValuePair<string, string>("Mali","ML"),
            new KeyValuePair<string, string>("Malta","MT"),
            new KeyValuePair<string, string>("Marshall Islands","MH"),
            new KeyValuePair<string, string>("Martinique","MQ"),
            new KeyValuePair<string, string>("Mauritania","MR"),
            new KeyValuePair<string, string>("Mauritius","MU"),
            new KeyValuePair<string, string>("Mayotte","YT"),
            new KeyValuePair<string, string>("Mexico","MX"),
            new KeyValuePair<string, string>("Micronesia (Federated States of)","FM"),
            new KeyValuePair<string, string>("Moldova","MD"),
            new KeyValuePair<string, string>("Monaco","MC"),
            new KeyValuePair<string, string>("Mongolia","MN"),
            new KeyValuePair<string, string>("Montenegro","ME"),
            new KeyValuePair<string, string>("Montserrat","MS"),
            new KeyValuePair<string, string>("Morocco","MA"),
            new KeyValuePair<string, string>("Mozambique","MZ"),
            new KeyValuePair<string, string>("Myanmar","MM"),
            new KeyValuePair<string, string>("Namibia","NA"),
            new KeyValuePair<string, string>("Nauru","NR"),
            new KeyValuePair<string, string>("Nepal","NP"),
            new KeyValuePair<string, string>("Netherlands","NL"),
            new KeyValuePair<string, string>("Netherlands Antilles","AN"),
            new KeyValuePair<string, string>("New Caledonia","NC"),
            new KeyValuePair<string, string>("New Zealand","NZ"),
            new KeyValuePair<string, string>("Nicaragua","NI"),
            new KeyValuePair<string, string>("Niger","NE"),
            new KeyValuePair<string, string>("Nigeria","NG"),
            new KeyValuePair<string, string>("Niue","NU"),
            new KeyValuePair<string, string>("Norfolk Island","NF"),
            new KeyValuePair<string, string>("Northern Mariana Islands","MP"),
            new KeyValuePair<string, string>("Norway","NO"),
            new KeyValuePair<string, string>("Oman","OM"),
            new KeyValuePair<string, string>("Pakistan","PK"),
            new KeyValuePair<string, string>("Palau","PW"),
            new KeyValuePair<string, string>("Palestine","PS"),
            new KeyValuePair<string, string>("Panama","PA"),
            new KeyValuePair<string, string>("Papua New Guinea","PG"),
            new KeyValuePair<string, string>("Paraguay","PY"),
            new KeyValuePair<string, string>("Peru","PE"),
            new KeyValuePair<string, string>("Philippines","PH"),
            new KeyValuePair<string, string>("Pitcairn","PN"),
            new KeyValuePair<string, string>("Poland","PL"),
            new KeyValuePair<string, string>("Portugal","PT"),
            new KeyValuePair<string, string>("Puerto Rico","PR"),
            new KeyValuePair<string, string>("Qatar","QA"),
            new KeyValuePair<string, string>("Réunion","RE"),
            new KeyValuePair<string, string>("Romania","RO"),
            new KeyValuePair<string, string>("Russia","RU"),
            new KeyValuePair<string, string>("Rwanda","RW"),
            new KeyValuePair<string, string>("Saint Helena","SH"),
            new KeyValuePair<string, string>("Saint Kitts And Nevis","KN"),
            new KeyValuePair<string, string>("Saint Lucia","LC"),
            new KeyValuePair<string, string>("Saint Pierre and Miquelon","PM"),
            new KeyValuePair<string, string>("Saint Vincent and the Grenadines","VC"),
            new KeyValuePair<string, string>("Samoa","WS"),
            new KeyValuePair<string, string>("San Marino","SM"),
            new KeyValuePair<string, string>("Sao Tome and Principe","ST"),
            new KeyValuePair<string, string>("Saudi Arabia","SA"),
            new KeyValuePair<string, string>("Senegal","SN"),
            new KeyValuePair<string, string>("Serbia","RS"),
            new KeyValuePair<string, string>("Seychelles","SC"),
            new KeyValuePair<string, string>("Sierra Leone","SL"),
            new KeyValuePair<string, string>("Singapore","SG"),
            new KeyValuePair<string, string>("Slovakia","SK"),
            new KeyValuePair<string, string>("Slovenia","SI"),
            new KeyValuePair<string, string>("Solomon Islands","SB"),
            new KeyValuePair<string, string>("Somalia","SO"),
            new KeyValuePair<string, string>("South Africa","ZA"),
            new KeyValuePair<string, string>("South Georgia and the South Sandwich Islands","GS"),
            new KeyValuePair<string, string>("Spain","ES"),
            new KeyValuePair<string, string>("Sri Lanka","LK"),
            new KeyValuePair<string, string>("Sudan","SD"),
            new KeyValuePair<string, string>("Suriname","SR"),
            new KeyValuePair<string, string>("Svalbard and Jan Mayen","SJ"),
            new KeyValuePair<string, string>("Swaziland","SZ"),
            new KeyValuePair<string, string>("Sweden","SE"),
            new KeyValuePair<string, string>("Switzerland","CH"),
            new KeyValuePair<string, string>("Syria","SY"),
            new KeyValuePair<string, string>("Taiwan","TW"),
            new KeyValuePair<string, string>("Tajikistan","TJ"),
            new KeyValuePair<string, string>("Tanzania","TZ"),
            new KeyValuePair<string, string>("Thailand","TH"),
            new KeyValuePair<string, string>("Timor-Leste","TL"),
            new KeyValuePair<string, string>("Togo","TG"),
            new KeyValuePair<string, string>("Tokelau","TK"),
            new KeyValuePair<string, string>("Tonga","TO"),
            new KeyValuePair<string, string>("Trinidad","TT"),
            new KeyValuePair<string, string>("Tunisia","TN"),
            new KeyValuePair<string, string>("Turkey","TR"),
            new KeyValuePair<string, string>("Turkmenistan","TM"),
            new KeyValuePair<string, string>("Turks and Caicos Islands","TC"),
            new KeyValuePair<string, string>("Tuvalu","TV"),
            new KeyValuePair<string, string>("Uganda","UG"),
            new KeyValuePair<string, string>("Ukraine","UA"),
            new KeyValuePair<string, string>("United Arab Emirates","AE"),
            new KeyValuePair<string, string>("United Kingdom","GB"),
            new KeyValuePair<string, string>("United States","US"),
            new KeyValuePair<string, string>("United States Minor Outlying Islands","UM"),
            new KeyValuePair<string, string>("Uruguay","UY"),
            new KeyValuePair<string, string>("Uzbekistan","UZ"),
            new KeyValuePair<string, string>("Vanuatu","VU"),
            new KeyValuePair<string, string>("Venezuela","VE"),
            new KeyValuePair<string, string>("Vietnam","VN"),
            new KeyValuePair<string, string>("Virgin Islands (British)","VG"),
            new KeyValuePair<string, string>("Virgin Islands (U.S.)","VI"),
            new KeyValuePair<string, string>("Wallis and Futuna","WF"),
            new KeyValuePair<string, string>("Western Sahara","EH"),
            new KeyValuePair<string, string>("Yemen","YE"),
            new KeyValuePair<string, string>("Zambia","ZM"),
            new KeyValuePair<string, string>("Zimbabwe","ZW")
        };
            #endregion

            var myValue = data.FirstOrDefault(x => x.Key == sCountry).Value;

            return myValue;
        }

        private string GetRandomdate(string sAge)
        {
            string retunDate = string.Empty;
            DateTime RandDate = new DateTime();
            if (sAge == "15-24")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/2001", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1992", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1992", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "25-34")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1991", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1982", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1982", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "35-44")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1981", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1972", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1972", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "45-54")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1971", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1962", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1962", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "55-64")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1961", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1952", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1952", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1951", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1930", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1930", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }

            return retunDate;
        }

        private int InsertOAuthToken(int RegID, string access_token, string instance_url, string id, string token_type, string issued_at, string signature)
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.Parameters.Clear();
                        cmd.CommandText = @"Insert into PaymentDetails(RegID,access_token,instance_url,id,token_type,issued_at,signature)
                                        Values(@RegID,@access_token,@instance_url,@id,@token_type,@issued_at,@signature)" + "Select Scope_Identity()";
                        cmd.Parameters.Add(new SqlParameter("@RegID", RegID));
                        cmd.Parameters.Add(new SqlParameter("@access_token", access_token != null ? access_token : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@instance_url", instance_url != null ? instance_url : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@id", id != null ? id : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@token_type", token_type != null ? token_type : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@issued_at", issued_at != null ? issued_at : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@signature", signature != null ? signature : string.Empty));
                        int status = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                        cmd.Parameters.Clear();
                        return status;
                    }
                }
                catch (Exception ex)
                {

                    ErrorLogger.Log("AddAttendee.cs", "InsertOAuthToken", ex, "HttpPost");
                    return 0;
                }
            }

        }

        private void UpdateDMCCProlianceServices(int RegID, string TransactionReferenceNumber, string TransactionId, string Status, string ErrorMessage, string ErrorCode, string AccountNumber, string AccountName)
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.CommandText = @"UPDATE PaymentDetails  set 
                                            TransactionReferenceNumber=@TransactionReferenceNumber,
                                            TransactionId=@TransactionId,
                                            Status=@Status,
                                            ErrorMessage=@ErrorMessage,
                                            ErrorCode=@ErrorCode,
                                            AccountNumber=@AccountNumber,
                                            AccountName=@AccountName 
                                             where RegID=@RegID";
                        cmd.Parameters.Add(new SqlParameter("@RegID", RegID));
                        cmd.Parameters.Add(new SqlParameter("@TransactionReferenceNumber", TransactionReferenceNumber != null ? TransactionReferenceNumber : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@TransactionId", TransactionId != null ? TransactionId : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@Status", Status != null ? Status : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@ErrorMessage", ErrorMessage != null ? ErrorMessage : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@ErrorCode", ErrorCode != null ? ErrorCode : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@AccountNumber", AccountNumber != null ? AccountNumber : string.Empty));
                        cmd.Parameters.Add(new SqlParameter("@AccountName", AccountName != null ? AccountName : string.Empty));
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();

                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("AddAttendee.cs", "UpdateDMCCProlianceServices", ex, "HttpPost");
                }
            }

        }
        private void UpdateStatus()
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        cmd.CommandText = "Update Registrations set status = 0 where status='-2'  and  DATEDIFF(MI,DateRegister, getdate()) >=30";
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("AddAttendee.cs", "UpdateStatus", ex, "HttpPost");
                    divError.Attributes.Add("style", "display:block");
                    litMessage.Text = ex.Message.ToString();
                }
            }
        }
        #endregion

    }
}