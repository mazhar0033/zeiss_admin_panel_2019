﻿<%@ Page Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AddFreeAttendee.aspx.cs" Inherits="AdministrationPanel.AddFreeAttendee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LocalScript" runat="server">
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
    <form action="" method="post" id="from1">
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <i class="fa ion-card"></i>
                            <h3 class="box-title">Add Free Attendee</h3>
                        </div>
                        <div class="alert alert-danger" role="alert" id="divError" runat="server" style="display: none; padding: 0px; font-weight: bold;">
                            <asp:Literal ID="litMessage" runat="server" />
                            <span id="msg"></span>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                
                                <div class="col-xs-4">
                                    <label>First Name<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvFirstName" runat="server" Display="Dynamic" ControlToValidate="txtFirstName" ErrorMessage="Please enter First Name" ForeColor="Red" />
                                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="info" ID="revFirstName" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Please enter First Name" ValidationExpression="^[a-zA-Z\s\u0600-\u06FF]+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtFirstName" runat="server" placeholder="Full Name" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Last Name<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvLastName" runat="server" Display="Dynamic" ControlToValidate="txtLastName" ErrorMessage="Please enter Last Name" ForeColor="Red" />
                                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="info" ID="revLastName" runat="server" ControlToValidate="txtLastName" ErrorMessage="Please enter Last Name" ValidationExpression="^[a-zA-Z\s\u0600-\u06FF]+$" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:TextBox ID="txtLastName" runat="server" placeholder="Full Name" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Designation</label>
                                    <%--<asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvJobTitle" runat="server" Display="Dynamic" ControlToValidate="txtJobTitle" ErrorMessage="Please Enter Position " ForeColor="Red" />--%>
                                    <asp:TextBox runat="server" ID="txtDesignation" placeholder="Designation" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Company:<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvCompany" runat="server" Display="Dynamic" ControlToValidate="txtCompany" ErrorMessage="Please enter Company Name" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtCompany" placeholder="Company/Institution/Organization" CssClass="form-control" />
                                </div>
                                <div class="col-xs-4">
                                    <label>Nationality<span style="color: red;"> **</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="RequiredFieldValidator6" SetFocusOnError="true" runat="server" Display="Dynamic" ControlToValidate="ddlNationality" ErrorMessage="Please select a Nationality" ForeColor="Red" />
                                    <div class="field">
                                        <asp:DropDownList runat="server" ID="ddlNationality" CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <label>Country of Residence<span style="color: red;"> **</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" ID="RequiredFieldValidator7" SetFocusOnError="true" runat="server" Display="Dynamic" ControlToValidate="ddlCountry" ErrorMessage="Please select a Country of Residence" ForeColor="Red" />
                                    <div class="field">
                                        <asp:DropDownList runat="server" ID="ddlCountry" CssClass="form-control" />
                                    </div>
                                </div>
                                
                                <div class="col-xs-4">
                                    <label>City of Residence:<span style="color: red;"> *</span></label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="RequiredFieldValidator3" runat="server" Display="Dynamic" ControlToValidate="txtCity" ErrorMessage="Please enter City Name" ForeColor="Red" />
                                    <asp:TextBox runat="server" ID="txtCity" placeholder="City" CssClass="form-control" />
                                </div>

                                <div class="col-xs-4">
                                    <label>Email<span style="color: red;"> **</span> </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="true" ID="rfvEmail" runat="server" Display="Dynamic" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid Email Address" ForeColor="Red" />
                                    <asp:RegularExpressionValidator Display="Dynamic" SetFocusOnError="true" ValidationGroup="info" ID="validEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Please enter a valid Email Address" ValidationExpression="^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9]{2,}(?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+(?:[a-zA-Z]{2,})\b$" ForeColor="Red"></asp:RegularExpressionValidator>
                                    <asp:TextBox runat="server" ID="txtEmail" placeholder="Email" CssClass="form-control" />
                                </div>
                                
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Are you employed in the Private Sector or Public Sector?<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="True" ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="rdotickettype" ErrorMessage="Please Select" ForeColor="Red" />
                                    <asp:RadioButtonList ID="rdoSector" runat="server" RepeatDirection="Vertical">
                                        <asp:ListItem Value="Private Sector">Private Sector</asp:ListItem>
                                        <asp:ListItem Value="Public Sector">Public Sector</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-xs-4">
                                    <label>Are you registering as a Visitor or Exhibitor?<span style="color: red;"> *</span>  </label>
                                    <asp:RequiredFieldValidator ValidationGroup="info" SetFocusOnError="True" ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ControlToValidate="rdotickettype" ErrorMessage="Please Select" ForeColor="Red" />
                                    <asp:RadioButtonList ID="rdotickettype" runat="server" RepeatDirection="Vertical">
                                        <asp:ListItem Value="Visitor">Visitor</asp:ListItem>
                                        <asp:ListItem Value="Exhibitor">Exhibitor</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-xs-12">
                                    <label>Comments</label>
                                    <asp:TextBox runat="server" ID="txtComments" placeholder="Comments" Style="width: 100%;" TextMode="MultiLine" />
                                </div>
                            </div>
                            <div class="box-footer">
                                <asp:Button ID="btnRegister" runat="server" ValidationGroup="info" Text="Register" CssClass="btn btn-info" OnClick="btnRegister_Click" />

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#aspnetForm").submit(function () {
                var selected = $("#ctl00_Body_ddlMobileCountry option:selected").text();
                var myLength = $("#ctl00_Body_txtMobileNumber").val().length;
                if (selected == "United Arab Emirates (971)" && myLength < 9) {
                    $("#ctl00_Body_txtMobileNumber").focus();
                    $("#erroUAEmobile").text("Please enter valid mobile number (minimum 9)");
                    return false;
                }
                else {
                    $("#erroUAEmobile").text("");
                }
            });

            $("#ctl00_Body_txtMobileNumber").focusout(function () {
                var s = $("ctl00_Body_#txtMobileNumber").val();
                $("#ctl00_Body_txtMobileNumber").val(s.replace(/^(0+)/g, ''));
            });

            $("#ctl00_Body_txtMobileNumber").keyup(function () {
                var numbers = $(this).val();
                $(this).val(numbers.replace(/\D/, ''));
            });

            $("#ctl00_Body_ddlCountry").change(function () {
                var selectedIndex = parseInt(this.selectedIndex);
                $('#ctl00_Body_ddlMobileCountry').val(selectedIndex);
                $("#ctl00_Body_txtMobileNumber").val("");
                $("#erroUAEmobile").text("");
                if (selectedIndex == "1") {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 9);

                }
                else {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 15);
                }
            });

            $("#ctl00_Body_ddlMobileCountry").change(function () {
                var selected = $("#ctl00_Body_ddlMobileCountry option:selected").text();
                $("#erroUAEmobile").text("");
                if (selected == "United Arab Emirates (971)") {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").val("");
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 9);
                }
                else {
                    $("#fax-area").show();
                    $("#ctl00_Body_txtMobileNumber").val("");
                    $("#ctl00_Body_txtMobileNumber").prop('maxLength', 15);
                }
            });

        });
    </script>

    <script type="text/javascript">
        function preventMultipleSubmissions() {
            $('#<%=btnRegister.ClientID %>').prop('disabled', true);
        }
        window.onbeforeunload = preventMultipleSubmissions;
    </script>
</asp:Content>
