﻿using iTextSharp.text.pdf;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Drawing;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.Caching;
using Microsoft.Practices.EnterpriseLibrary.Caching.Expirations;
using OnlineRegistration.Logger;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZXing;
using ZXing.Common;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Globalization;
using Ionic.Zip;
using System.Configuration;
using System.Web.Configuration;

namespace AdministrationPanel
{
    public partial class registrations : System.Web.UI.Page
    {
        protected MemberInfo member = null;
        //ICacheManager DTCMCache = CacheFactory.GetCacheManager("DTCM");
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
                string UserType = this.member.Type;
                if (UserType != "SuperAdmin")
                {
                    btnGenerateCSV.Visible = false;
                }
                this.Page.Title = "Admin - Registrations";
                if (!IsPostBack)
                {
                    PopulateCountry();
                    string QueryCountry = Request.QueryString.Get("country");
                    string business = Request.QueryString.Get("business");
                    string Country = Request.QueryString.Get("Country");
                    string Nationality = Request.QueryString.Get("Nationality");
                    string welcome = Request.QueryString.Get("welcome");
                    string hotel = Request.QueryString.Get("hotel");
                    if (QueryCountry != null)
                    {
                        FilterCountry(QueryCountry);
                        btnGenerateCSV.Visible = false;
                    }
                    else if (business != null)
                    {
                        FilterQuestion("businessdinner", business);
                        btnGenerateCSV.Visible = false;
                    }
                    else if (Country != null)
                    {
                        FilterQuestion("Country", Country);
                        btnGenerateCSV.Visible = false;
                    }
                    else if (Nationality != null)
                    {
                        FilterQuestion("Nationality", Nationality);
                        btnGenerateCSV.Visible = false;
                    }
                    else if (welcome != null)
                    {
                        FilterQuestion("Welcomedinner", welcome);
                        btnGenerateCSV.Visible = false;
                    }
                    else if (welcome != null)
                    {
                        FilterQuestion("hotelreservation", hotel);
                        btnGenerateCSV.Visible = false;
                    }
                    else
                        PopulateGV();
                }
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Registrations.cs", "Load", ex, "HttpGet");
            }


        }
        private void PopulateCountry()
        {
            DataTable country = new DataTable();
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {
                    SqlDataAdapter adapter = new SqlDataAdapter("SELECT ID, Country FROM Country", con);
                    adapter.Fill(country);
                    ddlCountry.DataSource = country;
                    ddlCountry.DataTextField = "Country";
                    ddlCountry.DataValueField = "ID";
                    ddlCountry.DataBind();

                    ddlNationality.DataSource = country;
                    ddlNationality.DataTextField = "Country";
                    ddlNationality.DataValueField = "ID";
                    ddlNationality.DataBind();
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("Registrations.cs", "PopulateCountry", ex, "HttpGet");
                }
            }
            // Add the initial item - you can add this even if the options from the
            // db were not successfully loaded
            ddlNationality.Items.Insert(0, new ListItem(string.Empty, "0"));
            ddlCountry.Items.Insert(0, new ListItem(string.Empty, "0"));

        }
        private void FilterQuestion(string p, string Answer)
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"SELECT ID,title,
	       FirstName COLLATE SQL_Latin1_General_CP1_CI_AS + ' ' + LastName COLLATE SQL_Latin1_General_CP1_CI_AS As Name
		   
      
      ,[Email]
      ,[Country]
      ,[Nationality]
      ,[Hospital]
      ,[City]
      ,[WelcomeDinner]
      ,[BusinessDinner]
      ,[HotelReservation]
      ,[DateTime]
      ,[IsAgree]
		   From Registrations where " + p + " like '%" + Answer + "%'";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        int count = dSet.Tables[0].Rows.Count;
                        ViewState["Count"] = count;
                        totalCount.Text = "Total - " + count.ToString();
                        ViewState["Table"] = dSet;
                        gvRegistrations.DataSource = dSet.Tables["Table"].DefaultView;
                        gvRegistrations.DataBind();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("Registrations.cs", "FilterQuestion", ex, "HttpGet");
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }
        private void FilterCountry(string QueryCountry)
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"SELECT ID,title,
	       FirstName COLLATE SQL_Latin1_General_CP1_CI_AS + ' ' + LastName COLLATE SQL_Latin1_General_CP1_CI_AS As Name
		   
      
      ,[Email]
      ,[Country]
      ,[Nationality]
      ,[Hospital]
      ,[City]
      ,[WelcomeDinner]
      ,[BusinessDinner]
      ,[HotelReservation]
      ,[DateTime]
      ,[IsAgree]
		   From Registrations where  Country like '%" + QueryCountry + "%'";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        int count = dSet.Tables[0].Rows.Count;
                        ViewState["Count"] = count;
                        totalCount.Text = "Total - " + count.ToString();
                        ViewState["Table"] = dSet;
                        gvRegistrations.DataSource = dSet.Tables["Table"].DefaultView;
                        gvRegistrations.DataBind();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("Registrations.cs", "FilterCountry", ex, "HttpGet");
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }
        private void PopulateGV()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"exec PopulateGridView";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        int count = dSet.Tables[0].Rows.Count;
                        ViewState["Count"] = count;
                        totalCount.Text = "Total - " + count.ToString();
                        ViewState["Table"] = dSet;
                        gvRegistrations.DataSource = dSet.Tables["Table"].DefaultView;
                        gvRegistrations.DataBind();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("Registrations.cs", "PopulateGV", ex, "HttpGet");
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }
        protected void gvRegistrations_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //RememberOldValues();
            gvRegistrations.PageIndex = e.NewPageIndex;
            DataSet ds = (DataSet)ViewState["Table"];
            gvRegistrations.DataSource = ds;
            gvRegistrations.DataBind();
            RePopulateValues();
        }
        private void RememberOldValues()
        {
            ArrayList categoryIDList = new ArrayList();
            int index = -1;
            foreach (GridViewRow row in gvRegistrations.Rows)
            {
                index = (int)gvRegistrations.DataKeys[row.RowIndex].Value;
                bool result = ((CheckBox)row.FindControl("chkRow")).Checked;

                // Check in the Session
                if (Session["CHECKED_ITEMS"] != null)
                    categoryIDList = (ArrayList)Session["CHECKED_ITEMS"];
                if (result)
                {
                    if (!categoryIDList.Contains(index))
                        categoryIDList.Add(index);
                }
                else
                    categoryIDList.Remove(index);
            }
            if (categoryIDList != null && categoryIDList.Count > 0)
                Session["CHECKED_ITEMS"] = categoryIDList;
        }
        private void RePopulateValues()
        {
            ArrayList categoryIDList = (ArrayList)Session["CHECKED_ITEMS"];
            if (categoryIDList != null && categoryIDList.Count > 0)
            {
                foreach (GridViewRow row in gvRegistrations.Rows)
                {
                    int index = (int)gvRegistrations.DataKeys[row.RowIndex].Value;
                    if (categoryIDList.Contains(index))
                    {
                        CheckBox myCheckBox = (CheckBox)row.FindControl("chkRow");
                        myCheckBox.Checked = true;
                    }
                }
            }
        }
        protected void btnGenerateCSV_Click(object sender, EventArgs e)
        {

            string sFirstName = txtFirstName.Text;
            string sLastName = txtLastName.Text;
            string sCountry = ddlCountry.SelectedItem.Text;
            string sNationality = ddlNationality.SelectedItem.Text;
            string sEmail = txtEmail.Text;
            string welcome = ddlWelcome.SelectedValue;
            string business = ddlBusiness.SelectedValue;
            string hotel = ddlHotel.SelectedValue;
            string sDate = txtdate.Text != string.Empty ? txtdate.Text : string.Empty;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("SearchGenerateCSV", conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = null;
                    DataTable DtSet = new DataTable();
                    cmd.Parameters.Add(new SqlParameter("@FirstName", sFirstName));
                    cmd.Parameters.Add(new SqlParameter("@LastName", sLastName));
                    cmd.Parameters.Add(new SqlParameter("@Country", sCountry));
                    cmd.Parameters.Add(new SqlParameter("@Nationality", sNationality));
                    cmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                    cmd.Parameters.Add(new SqlParameter("@Welcome", welcome));
                    cmd.Parameters.Add(new SqlParameter("@Business", business));
                    cmd.Parameters.Add(new SqlParameter("@Hotel", hotel));
                    cmd.Parameters.Add(new SqlParameter("@Date", sDate != string.Empty ? Convert.ToDateTime(sDate).ToString("yyyy-MM-dd") : string.Empty));
                    da = new SqlDataAdapter(cmd);
                    da.Fill(DtSet);
                    Response.Clear();
                    Response.ClearContent();
                    Response.ContentType = "application/octet-stream";
                    Response.AddHeader("Content-Disposition", "attachment; filename=Zeiss2019Reg.xls");
                    Response.ContentEncoding = System.Text.Encoding.Unicode;
                    Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
                    // Create a dynamic control, populate and render it
                    GridView excel = new GridView();
                    excel.DataSource = DtSet;
                    excel.DataBind();
                    excel.RenderControl(new HtmlTextWriter(Response.Output));
                    Response.Flush();
                    Response.End();

                }

            }
        }
        protected void btnEmailSubmit_Click(object sender, EventArgs e)
        {
            ArrayList categoryIDList = new ArrayList();
            int index = -1;
            foreach (GridViewRow row in gvRegistrations.Rows)
            {
                index = (int)gvRegistrations.DataKeys[row.RowIndex].Value;
                bool result = ((CheckBox)row.FindControl("chkRow")).Checked;

                // Check in the Session
                if (Session["CHECKED_ITEMS"] != null)
                    categoryIDList = (ArrayList)Session["CHECKED_ITEMS"];
                if (result)
                {
                    if (!categoryIDList.Contains(index))
                        categoryIDList.Add(index);
                }
                else
                    categoryIDList.Remove(index);
            }
            if (categoryIDList.Count > 0)
            {
                Session["CHECKED_ITEMS"] = categoryIDList;

                string CombineEmail = String.Join(",", categoryIDList.ToArray());
                string emails = GetEmailAddress(CombineEmail);
                string sSubject = txtEmailSubject.Text;
                string sMailBody = txtEmailMessage.Value;
                MailMessage mail = new MailMessage();
                string mailTo = emails;
                mail.To.Add(mailTo);
                if (sSubject == null || sSubject == "")
                {
                    mail.Subject = "THE DUBAI PRECIOUS METALS CONFERENCE";
                }
                else
                {
                    mail.Subject = sSubject;
                }
                mail.From = new MailAddress("events@dmcc.ae", "DPMC 2019 - Registration");

                mail.IsBodyHtml = true;
                SmtpClient client = new SmtpClient("smtp.sendgrid.net", 587);
                client.Credentials = new NetworkCredential("mohsinxmushtaq", "mohsin@123");
                mail.Body = sMailBody;
                client.Send(mail);
            }
            Response.Redirect(Request.RawUrl);
        }
        private string GetEmailAddress(string CombineEmail)
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                string EmailAddress = string.Empty;
                using (SqlCommand cmd = new SqlCommand())
                {
                    List<string> emails = new List<string>();
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = "Select Email from Registrations where RegID in(" + CombineEmail + ")";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        DataTable dt = dSet.Tables["Table"];
                        foreach (DataRow dr in dt.Rows)
                        {
                            string Email = (String)dr["Email"];
                            emails.Add(Email);
                        }
                        EmailAddress = string.Join<string>(",", emails);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("Registrations.cs", "GetEmailAddress", ex, "HttpGet");
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                    return EmailAddress;
                }

            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string sFirstName = txtFirstName.Text;
            string sLastName = txtLastName.Text;
            string sCountry = ddlCountry.SelectedItem.Text;
            string sNationality = ddlNationality.SelectedItem.Text;
            string sEmail = txtEmail.Text;
            string welcome = ddlWelcome.SelectedValue;
            string business = ddlBusiness.SelectedValue;
            string hotel = ddlHotel.SelectedValue;
            string sDate = txtdate.Text != string.Empty ? txtdate.Text : string.Empty;



            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("SearchReg", conn))
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        cmd.Parameters.Add(new SqlParameter("@FirstName", sFirstName));
                        cmd.Parameters.Add(new SqlParameter("@LastName", sLastName));
                        cmd.Parameters.Add(new SqlParameter("@Country", sCountry));
                        cmd.Parameters.Add(new SqlParameter("@Nationality", sNationality));
                        cmd.Parameters.Add(new SqlParameter("@Email", sEmail));
                        cmd.Parameters.Add(new SqlParameter("@Welcome", welcome));
                        cmd.Parameters.Add(new SqlParameter("@Business", business));
                        cmd.Parameters.Add(new SqlParameter("@Hotel", hotel));
                        cmd.Parameters.Add(new SqlParameter("@Date", sDate != string.Empty ? Convert.ToDateTime(sDate).ToString("yyyy-MM-dd") : string.Empty));
                        da = new SqlDataAdapter(cmd);
                        da.Fill(dSet, "Table");
                        int count = dSet.Tables[0].Rows.Count;
                        ViewState["Count"] = count;
                        totalCount.Text = "Total - " + count.ToString();
                        ViewState["Table"] = dSet;
                        gvRegistrations.DataSource = dSet.Tables["Table"].DefaultView;
                        gvRegistrations.DataBind();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.Log("Registrations.cs", "btnSearch_Click", ex, "HttpGet");
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }

        }
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            gvRegistrations.AllowPaging = false;
            DataSet ds = (DataSet)ViewState["Table"];
            gvRegistrations.DataSource = ds;
            gvRegistrations.DataBind();
            foreach (GridViewRow gvRow in gvRegistrations.Rows)
            {
                CheckBox chkSel = (CheckBox)gvRow.FindControl("chkRow");
                chkSel.Checked = true;
            }
            gridReg.Attributes.Add("style", "overflow-y: scroll; height:400px;");
        }
        private void UpdateEditLogs(int Order_number)
        {
            int sVisitorID = Order_number;
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string sUserName = this.member.Name;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandText = @"insert into EditLogs(VisitorId,DateUpdated,Username) values(@VisitorId,@DateUpdated,@Username)";
                    cmd.Parameters.Add(new SqlParameter("@VisitorId", sVisitorID));
                    cmd.Parameters.Add(new SqlParameter("@DateUpdated", DateTime.Now));
                    cmd.Parameters.Add(new SqlParameter("@Username", sUserName));
                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
        }
        private void UpdateStatus(string Order_number, string Status)
        {
            using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                try
                {

                    con.Open();
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandTimeout = 500;
                        //Update Detials
                        if (Status == "Approve")
                        {
                            cmd.CommandText = @" UPDATE Registrations SET Status = 2  WHERE (Registrations.ID = @InRegID)";
                        }
                        else
                        {
                            cmd.CommandText = @" UPDATE Registrations SET Status = 3  WHERE (Registrations.ID = @InRegID)";
                        }
                        cmd.Parameters.Add(new SqlParameter("@InRegID", Order_number));
                        cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
                }
                catch (Exception ex)
                {
                    ErrorLogger.Log("Registrations.cs", "UpdateStatus", ex, "HttpGet");
                }
            }
        }
        private void SendEmail(string visitorID, string FullName, string Email, string Status)
        {
            try
            {
                string fromEmail = WebConfigurationManager.AppSettings["FromEmail"].ToString();
                string DisplayName = WebConfigurationManager.AppSettings["DisplayName"].ToString();
                int SMTPPort = Convert.ToInt32(WebConfigurationManager.AppSettings["SMTPPort"].ToString());
                string SMTPHost = WebConfigurationManager.AppSettings["SMTPHost"].ToString();
                string SMTPUsername = WebConfigurationManager.AppSettings["SMTPUsername"].ToString();
                string SMTPPassword = WebConfigurationManager.AppSettings["SMTPPassword"].ToString();


                MailMessage mail = new MailMessage();
                string mailTo = Email;
                mail.To.Add(mailTo);
                mail.From = new MailAddress(fromEmail, DisplayName);
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
                SmtpClient client = new SmtpClient(SMTPHost, SMTPPort);
                client.Credentials = new NetworkCredential(SMTPUsername, SMTPPassword);

                string strPath = "";
                if (Status == "Approve")
                {
                    strPath = Server.MapPath("ConfirmationEmail.html");
                    mail.Subject = "Your Registration Confirmation for the Conference";
                }
                else
                {
                    strPath = Server.MapPath("DeclinedEmail.html");
                    mail.Subject = "Your Registration Declined for the Conference";
                }
                string strContent = System.IO.File.ReadAllText(strPath);
                strContent = strContent.Replace("{ID}", visitorID);
                strContent = strContent.Replace("{name}", FullName);
                mail.Body = strContent;
                client.Send(mail);
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Registrations.cs", "SendEmail", ex, "HttpGet");
            }
        }

        #region DTCMRelated
        class CacheLogPolicy : ICacheItemRefreshAction
        {
            //Implement Referesh Method of interface ICacheItemRefreshAction
            public void Refresh(string removedKey, object expiredValue, Microsoft.Practices.EnterpriseLibrary.Caching.CacheItemRemovedReason removalReason)
            {

            }

        }

        private bool isSessionExists(string ChName)
        {
            //if (DTCMCache[ChName] != null)
            //    return true;
            //else
            return false;
        }

        private static IEnumerable<JToken> AllChildren(JToken json)
        {
            foreach (var c in json.Children())
            {
                yield return c;
                foreach (var cc in AllChildren(c))
                {
                    yield return cc;
                }
            }
        }

        private string GetCodeCountry(string sCountry)
        {
            KeyValuePair<string, string> kvpCountry = new KeyValuePair<string, string>();

            #region CountryCodes
            var data = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("Afghanistan","AF"),
            new KeyValuePair<string, string>("Aland Islands","AX"),
            new KeyValuePair<string, string>("Albania","AL"),
            new KeyValuePair<string, string>("Algeria","DZ"),
            new KeyValuePair<string, string>("American Samoa","AS"),
            new KeyValuePair<string, string>("Andorra","AD"),
            new KeyValuePair<string, string>("Angola","AO"),
            new KeyValuePair<string, string>("Anguilla","AI"),
            new KeyValuePair<string, string>("Antarctica","AQ"),
            new KeyValuePair<string, string>("Antigua and Barbuda","AG"),
            new KeyValuePair<string, string>("Argentina","AR"),
            new KeyValuePair<string, string>("Armenia","AM"),
            new KeyValuePair<string, string>("Aruba","AW"),
            new KeyValuePair<string, string>("Australia","AU"),
            new KeyValuePair<string, string>("Austria","AT"),
            new KeyValuePair<string, string>("Azerbaijan","AZ"),
            new KeyValuePair<string, string>("Bahamas","BS"),
            new KeyValuePair<string, string>("Bahrain","BH"),
            new KeyValuePair<string, string>("Bangladesh","BD"),
            new KeyValuePair<string, string>("Barbados","BB"),
            new KeyValuePair<string, string>("Belarus","BY"),
            new KeyValuePair<string, string>("Belgium","BE"),
            new KeyValuePair<string, string>("Belize","BZ"),
            new KeyValuePair<string, string>("Benin","BJ"),
            new KeyValuePair<string, string>("Bermuda","BM"),
            new KeyValuePair<string, string>("Bhutan","BT"),
            new KeyValuePair<string, string>("Bolivia","BO"),
            new KeyValuePair<string, string>("Bosnia","BA"),
            new KeyValuePair<string, string>("Botswana","BW"),
            new KeyValuePair<string, string>("Bouvet Island","BV"),
            new KeyValuePair<string, string>("Brazil","BR"),
            new KeyValuePair<string, string>("British Indian Ocean Territory","IO"),
            new KeyValuePair<string, string>("Brunei Darussalam","BN"),
            new KeyValuePair<string, string>("Bulgaria","BG"),
            new KeyValuePair<string, string>("Burkina Faso","BF"),
            new KeyValuePair<string, string>("Burundi","BI"),
            new KeyValuePair<string, string>("Cambodia","KH"),
            new KeyValuePair<string, string>("Cameroon","CM"),
            new KeyValuePair<string, string>("Canada","CA"),
            new KeyValuePair<string, string>("Cape Verde","CV"),
            new KeyValuePair<string, string>("Cayman Islands","KY"),
            new KeyValuePair<string, string>("Central African Republic","CF"),
            new KeyValuePair<string, string>("Chad","TD"),
            new KeyValuePair<string, string>("Chile","CL"),
            new KeyValuePair<string, string>("China","CN"),
            new KeyValuePair<string, string>("Christmas Island","CX"),
            new KeyValuePair<string, string>("Cocos (Keeling) Islands","CC"),
            new KeyValuePair<string, string>("Colombia","CO"),
            new KeyValuePair<string, string>("Comoros","KM"),
            new KeyValuePair<string, string>("Congo","CG"),
            new KeyValuePair<string, string>("Congo (The Democratic Republic of)","CD"),
            new KeyValuePair<string, string>("Cook Islands","CK"),
            new KeyValuePair<string, string>("Costa Rica","CR"),
            new KeyValuePair<string, string>("Côte d'Ivoire","CI"),
            new KeyValuePair<string, string>("Croatia","HR"),
            new KeyValuePair<string, string>("Cuba","CU"),
            new KeyValuePair<string, string>("Cyprus","CY"),
            new KeyValuePair<string, string>("Czech Republic","CZ"),
            new KeyValuePair<string, string>("Denmark","DK"),
            new KeyValuePair<string, string>("Djibouti","DJ"),
            new KeyValuePair<string, string>("Dominica","DM"),
            new KeyValuePair<string, string>("Dominican Republic","DO"),
            new KeyValuePair<string, string>("Ecuador","EC"),
            new KeyValuePair<string, string>("Egypt","EG"),
            new KeyValuePair<string, string>("El Salvador","SV"),
            new KeyValuePair<string, string>("Equatorial Guinea","GQ"),
            new KeyValuePair<string, string>("Eritrea","ER"),
            new KeyValuePair<string, string>("Estonia","EE"),
            new KeyValuePair<string, string>("Ethiopia","ET"),
            new KeyValuePair<string, string>("Falkland Islands (Malvinas)","FK"),
            new KeyValuePair<string, string>("Faroe Islands","FO"),
            new KeyValuePair<string, string>("Fiji","FJ"),
            new KeyValuePair<string, string>("Finland","FI"),
            new KeyValuePair<string, string>("France","FR"),
            new KeyValuePair<string, string>("French Guiana","GF"),
            new KeyValuePair<string, string>("French Polynesia","PF"),
            new KeyValuePair<string, string>("French Southern Territories","TF"),
            new KeyValuePair<string, string>("Gabon","GA"),
            new KeyValuePair<string, string>("Gambia","GM"),
            new KeyValuePair<string, string>("Georgia","GE"),
            new KeyValuePair<string, string>("Germany","DE"),
            new KeyValuePair<string, string>("Ghana","GH"),
            new KeyValuePair<string, string>("Gibraltar","GI"),
            new KeyValuePair<string, string>("Greece","GR"),
            new KeyValuePair<string, string>("Greenland","GL"),
            new KeyValuePair<string, string>("Grenada","GD"),
            new KeyValuePair<string, string>("Guadeloupe","GP"),
            new KeyValuePair<string, string>("Guam","GU"),
            new KeyValuePair<string, string>("Guatemala","GT"),
            new KeyValuePair<string, string>("Guernsey","GG"),
            new KeyValuePair<string, string>("Guinea","GN"),
            new KeyValuePair<string, string>("Guinea-Bissau","GW"),
            new KeyValuePair<string, string>("Guyana","GY"),
            new KeyValuePair<string, string>("Haiti","HT"),
            new KeyValuePair<string, string>("Heard Island and McDonald Islands","HM"),
            new KeyValuePair<string, string>("Holy See (Vatican City State)","VA"),
            new KeyValuePair<string, string>("Honduras","HN"),
            new KeyValuePair<string, string>("Hong Kong","HK"),
            new KeyValuePair<string, string>("Hungary","HU"),
            new KeyValuePair<string, string>("Iceland","IS"),
            new KeyValuePair<string, string>("India","IN"),
            new KeyValuePair<string, string>("Indonesia","ID"),
            new KeyValuePair<string, string>("Iran","IR"),
            new KeyValuePair<string, string>("Iraq","IQ"),
            new KeyValuePair<string, string>("Ireland","IE"),
            new KeyValuePair<string, string>("Isle of Man","IM"),
            new KeyValuePair<string, string>("Israel","IL"),
            new KeyValuePair<string, string>("Italy","IT"),
            new KeyValuePair<string, string>("Jamaica","JM"),
            new KeyValuePair<string, string>("Japan","JP"),
            new KeyValuePair<string, string>("Jersey","JE"),
            new KeyValuePair<string, string>("Jordan","JO"),
            new KeyValuePair<string, string>("Kazakhstan","KZ"),
            new KeyValuePair<string, string>("Kenya","KE"),
            new KeyValuePair<string, string>("Kiribati","KI"),
            new KeyValuePair<string, string>("Korea, Republic of","KR"),
            new KeyValuePair<string, string>("Korea (DPR)","KP"),
            new KeyValuePair<string, string>("Kuwait","KW"),
            new KeyValuePair<string, string>("Kyrgystan","KG"),
            new KeyValuePair<string, string>("Lao Peoples Democratic Republic","LA"),
            new KeyValuePair<string, string>("Latvia","LV"),
            new KeyValuePair<string, string>("Lebanon","LB"),
            new KeyValuePair<string, string>("Lesotho","LS"),
            new KeyValuePair<string, string>("Liberia","LR"),
            new KeyValuePair<string, string>("Libyan Arab Jamahiriya","LY"),
            new KeyValuePair<string, string>("Liechtenstein","LI"),
            new KeyValuePair<string, string>("Lithuania","LT"),
            new KeyValuePair<string, string>("Luxembourg","LU"),
            new KeyValuePair<string, string>("Macao","MO"),
            new KeyValuePair<string, string>("Macedonia (The Former Yugoslav Republic of)","MK"),
            new KeyValuePair<string, string>("Madagascar","MG"),
            new KeyValuePair<string, string>("Malawi","MW"),
            new KeyValuePair<string, string>("Malaysia","MY"),
            new KeyValuePair<string, string>("Maldives","MV"),
            new KeyValuePair<string, string>("Mali","ML"),
            new KeyValuePair<string, string>("Malta","MT"),
            new KeyValuePair<string, string>("Marshall Islands","MH"),
            new KeyValuePair<string, string>("Martinique","MQ"),
            new KeyValuePair<string, string>("Mauritania","MR"),
            new KeyValuePair<string, string>("Mauritius","MU"),
            new KeyValuePair<string, string>("Mayotte","YT"),
            new KeyValuePair<string, string>("Mexico","MX"),
            new KeyValuePair<string, string>("Micronesia (Federated States of)","FM"),
            new KeyValuePair<string, string>("Moldova","MD"),
            new KeyValuePair<string, string>("Monaco","MC"),
            new KeyValuePair<string, string>("Mongolia","MN"),
            new KeyValuePair<string, string>("Montenegro","ME"),
            new KeyValuePair<string, string>("Montserrat","MS"),
            new KeyValuePair<string, string>("Morocco","MA"),
            new KeyValuePair<string, string>("Mozambique","MZ"),
            new KeyValuePair<string, string>("Myanmar","MM"),
            new KeyValuePair<string, string>("Namibia","NA"),
            new KeyValuePair<string, string>("Nauru","NR"),
            new KeyValuePair<string, string>("Nepal","NP"),
            new KeyValuePair<string, string>("Netherlands","NL"),
            new KeyValuePair<string, string>("Netherlands Antilles","AN"),
            new KeyValuePair<string, string>("New Caledonia","NC"),
            new KeyValuePair<string, string>("New Zealand","NZ"),
            new KeyValuePair<string, string>("Nicaragua","NI"),
            new KeyValuePair<string, string>("Niger","NE"),
            new KeyValuePair<string, string>("Nigeria","NG"),
            new KeyValuePair<string, string>("Niue","NU"),
            new KeyValuePair<string, string>("Norfolk Island","NF"),
            new KeyValuePair<string, string>("Northern Mariana Islands","MP"),
            new KeyValuePair<string, string>("Norway","NO"),
            new KeyValuePair<string, string>("Oman","OM"),
            new KeyValuePair<string, string>("Pakistan","PK"),
            new KeyValuePair<string, string>("Palau","PW"),
            new KeyValuePair<string, string>("Palestine","PS"),
            new KeyValuePair<string, string>("Panama","PA"),
            new KeyValuePair<string, string>("Papua New Guinea","PG"),
            new KeyValuePair<string, string>("Paraguay","PY"),
            new KeyValuePair<string, string>("Peru","PE"),
            new KeyValuePair<string, string>("Philippines","PH"),
            new KeyValuePair<string, string>("Pitcairn","PN"),
            new KeyValuePair<string, string>("Poland","PL"),
            new KeyValuePair<string, string>("Portugal","PT"),
            new KeyValuePair<string, string>("Puerto Rico","PR"),
            new KeyValuePair<string, string>("Qatar","QA"),
            new KeyValuePair<string, string>("Réunion","RE"),
            new KeyValuePair<string, string>("Romania","RO"),
            new KeyValuePair<string, string>("Russia","RU"),
            new KeyValuePair<string, string>("Rwanda","RW"),
            new KeyValuePair<string, string>("Saint Helena","SH"),
            new KeyValuePair<string, string>("Saint Kitts And Nevis","KN"),
            new KeyValuePair<string, string>("Saint Lucia","LC"),
            new KeyValuePair<string, string>("Saint Pierre and Miquelon","PM"),
            new KeyValuePair<string, string>("Saint Vincent and the Grenadines","VC"),
            new KeyValuePair<string, string>("Samoa","WS"),
            new KeyValuePair<string, string>("San Marino","SM"),
            new KeyValuePair<string, string>("Sao Tome and Principe","ST"),
            new KeyValuePair<string, string>("Saudi Arabia","SA"),
            new KeyValuePair<string, string>("Senegal","SN"),
            new KeyValuePair<string, string>("Serbia","RS"),
            new KeyValuePair<string, string>("Seychelles","SC"),
            new KeyValuePair<string, string>("Sierra Leone","SL"),
            new KeyValuePair<string, string>("Singapore","SG"),
            new KeyValuePair<string, string>("Slovakia","SK"),
            new KeyValuePair<string, string>("Slovenia","SI"),
            new KeyValuePair<string, string>("Solomon Islands","SB"),
            new KeyValuePair<string, string>("Somalia","SO"),
            new KeyValuePair<string, string>("South Africa","ZA"),
            new KeyValuePair<string, string>("South Georgia and the South Sandwich Islands","GS"),
            new KeyValuePair<string, string>("Spain","ES"),
            new KeyValuePair<string, string>("Sri Lanka","LK"),
            new KeyValuePair<string, string>("Sudan","SD"),
            new KeyValuePair<string, string>("Suriname","SR"),
            new KeyValuePair<string, string>("Svalbard and Jan Mayen","SJ"),
            new KeyValuePair<string, string>("Swaziland","SZ"),
            new KeyValuePair<string, string>("Sweden","SE"),
            new KeyValuePair<string, string>("Switzerland","CH"),
            new KeyValuePair<string, string>("Syria","SY"),
            new KeyValuePair<string, string>("Taiwan","TW"),
            new KeyValuePair<string, string>("Tajikistan","TJ"),
            new KeyValuePair<string, string>("Tanzania","TZ"),
            new KeyValuePair<string, string>("Thailand","TH"),
            new KeyValuePair<string, string>("Timor-Leste","TL"),
            new KeyValuePair<string, string>("Togo","TG"),
            new KeyValuePair<string, string>("Tokelau","TK"),
            new KeyValuePair<string, string>("Tonga","TO"),
            new KeyValuePair<string, string>("Trinidad","TT"),
            new KeyValuePair<string, string>("Tunisia","TN"),
            new KeyValuePair<string, string>("Turkey","TR"),
            new KeyValuePair<string, string>("Turkmenistan","TM"),
            new KeyValuePair<string, string>("Turks and Caicos Islands","TC"),
            new KeyValuePair<string, string>("Tuvalu","TV"),
            new KeyValuePair<string, string>("Uganda","UG"),
            new KeyValuePair<string, string>("Ukraine","UA"),
            new KeyValuePair<string, string>("United Arab Emirates","AE"),
            new KeyValuePair<string, string>("United Kingdom","GB"),
            new KeyValuePair<string, string>("United States","US"),
            new KeyValuePair<string, string>("United States Minor Outlying Islands","UM"),
            new KeyValuePair<string, string>("Uruguay","UY"),
            new KeyValuePair<string, string>("Uzbekistan","UZ"),
            new KeyValuePair<string, string>("Vanuatu","VU"),
            new KeyValuePair<string, string>("Venezuela","VE"),
            new KeyValuePair<string, string>("Vietnam","VN"),
            new KeyValuePair<string, string>("Virgin Islands (British)","VG"),
            new KeyValuePair<string, string>("Virgin Islands (U.S.)","VI"),
            new KeyValuePair<string, string>("Wallis and Futuna","WF"),
            new KeyValuePair<string, string>("Western Sahara","EH"),
            new KeyValuePair<string, string>("Yemen","YE"),
            new KeyValuePair<string, string>("Zambia","ZM"),
            new KeyValuePair<string, string>("Zimbabwe","ZW")
        };
            #endregion

            var myValue = data.FirstOrDefault(x => x.Key == sCountry).Value;

            return myValue;
        }

        private string GetRandomdate(string sAge)
        {
            string retunDate = string.Empty;
            DateTime RandDate = new DateTime();
            if (sAge == "15-24")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/2001", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1992", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1992", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "25-34")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1991", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1982", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1982", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "35-44")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1981", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1972", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1972", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "45-54")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1971", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1962", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1962", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else if (sAge == "55-64")
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1961", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1952", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1952", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }
            else
            {
                TimeSpan timeSpan = DateTime.ParseExact("01/01/1951", "MM/dd/yyyy", CultureInfo.InvariantCulture) - DateTime.ParseExact("01/01/1930", "MM/dd/yyyy", CultureInfo.InvariantCulture);
                var randomTest = new Random();
                TimeSpan newSpan = new TimeSpan(0, randomTest.Next(0, Convert.ToInt32(timeSpan.TotalMinutes)), 0);
                RandDate = DateTime.ParseExact("01/01/1930", "MM/dd/yyyy", CultureInfo.InvariantCulture) + newSpan;
                retunDate = RandDate.ToString("yyyy-MM-dd");
            }

            return retunDate;
        }
        #endregion

        protected void btnDownloadInvoice_Click(object sender, EventArgs e)
        {
            try
            {
                string SourceFolderPath = (ConfigurationManager.AppSettings["SourceFolderPath"].ToString());
                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", "AbstractSumission" + ".zip"));
                bool recurseDirectories = true;
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddSelectedFiles("*.pdf", SourceFolderPath, string.Empty, recurseDirectories);
                    zip.Save(Response.OutputStream);
                }
                Response.End();
            }
            catch (Exception ex)
            {
                ErrorLogger.Log("Registrations.cs", "btnDownloadInvoice_Click", ex, "HttpGet");
            }
        }
        private void SendSMS(string sMobile, int id, string Status)
        {
            sMobile = sMobile.Replace(" ", "");
            string strPath = "";
            if (Status == "Approve")
            {
                strPath = Server.MapPath("ConfirmationSMS.html");
            }
            else
            {
                strPath = Server.MapPath("DeclinedSMS.html");
            }
            string sMessage = System.IO.File.ReadAllText(strPath);
            sMessage = sMessage.Replace("{ID}", id.ToString());

            string smsURL = "http://www.smartcall.ae/httpv2/submitHTTP.aspx?MessageText={1}&MobileNumber={0}&UserId=evento@smartcall.ae&Password=evt01&unicode=1";
            string strFull = string.Empty;
            strFull = string.Format(smsURL, sMobile, sMessage);
            string s = HttpUtility.HtmlEncode(strFull);
            WebRequest request = HttpWebRequest.Create(strFull);
            WebResponse response = request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());

            string urlText = reader.ReadToEnd();
        }

        
    }
}