﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace AdministrationPanel
{
    public partial class _default : System.Web.UI.Page
    {
        protected MemberInfo member = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string UserType = this.member.Type;
            if (UserType != "SuperAdmin")
            {
                btnGenerateCSV.Visible = false;
            }
           // GetCategory();
            GetTopCountry();
            GetTopNationality();
            getMapData();
            GetLast5Days();
            getWeekRegistrations();
            GetRegistrationsCount();
            GetIntlvslocal();
            GetGCCCountry();
            GetgvCategory();
            this.Page.Title = "Admin - Dashboard";
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                //Get Total Results
                var cmd = new SqlCommand("Total_Counts", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                var firstColumn = cmd.ExecuteScalar();
                cmd.CommandType = CommandType.StoredProcedure;
                firstColumn = cmd.ExecuteScalar();
                string TotalResult;
                if (firstColumn != null)
                {
                    TotalResult = firstColumn.ToString();
                    totalCounts.InnerText = TotalResult;
                }
            }

        }

        private void getWeekRegistrations()
        {
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandText = @"GetWeekReg";
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        sda.SelectCommand = cmd;
                        sda.Fill(dt);
                        string[] x = new string[dt.Rows.Count];
                        decimal[] y = new decimal[dt.Rows.Count];
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            x[i] = dt.Rows[i][0].ToString();
                            y[i] = Convert.ToInt32(dt.Rows[i][1]);
                        }
                        LineChart1.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y, Name = "2019 Registrations" });
                        LineChart1.CategoriesAxis = string.Join(",", x);
                        if (x.Length > 3)
                        {
                            LineChart1.ChartWidth = (x.Length * 75).ToString();
                        }
                        LineChart1.Visible = true;
                    }
                    cmd.Parameters.Clear();

                    //cmd.CommandText = @"GetWeekReg2017";
                    //using (SqlDataAdapter sda = new SqlDataAdapter())
                    //{
                    //    cmd.CommandType = CommandType.StoredProcedure;
                    //    sda.SelectCommand = cmd;
                    //    sda.Fill(dt1);
                    //    string[] x = new string[dt1.Rows.Count];
                    //    decimal[] y = new decimal[dt1.Rows.Count];
                    //    for (int i = 0; i < dt1.Rows.Count; i++)
                    //    {
                    //        x[i] = dt1.Rows[i][0].ToString();
                    //        y[i] = Convert.ToInt32(dt1.Rows[i][1]);
                    //    }
                    //    LineChart2.Series.Add(new AjaxControlToolkit.LineChartSeries { Data = y, Name = "2018 Registrations" });
                    //    LineChart2.CategoriesAxis = string.Join(",", x);
                    //    if (x.Length > 3)
                    //    {
                    //        LineChart2.ChartWidth = (x.Length * 75).ToString();
                    //    }
                    //    LineChart2.Visible = true;
                    //}

                    cmd.Parameters.Clear();
                }
            }
        }

        class Foo
        {
            public Dictionary<string, int> GetQuestions { get; set; }
        }

        private void getMapData()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataTable dt = new DataTable();
                    try
                    {
                        string sql = @"select c.Code as Country,Count(*) As Count from CountryCodes c
                                    inner join Registrations r on r.Country COLLATE SQL_Latin1_General_CP1_CI_AS = c.Country COLLATE SQL_Latin1_General_CP1_CI_AS
                                     group by C.Code";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dt);
                        var dictionary = dt.Rows.OfType<DataRow>().ToDictionary(d => d.Field<string>(0), v => v.Field<int>(1));
                        var f = new Foo
                        {
                            GetQuestions = dictionary
                        };
                        string jsonString = JsonConvert.SerializeObject(f);
                        jsonString = jsonString.Remove(0, 16);
                        jsonString = jsonString.Remove(jsonString.Length - 1);
                        txtResult.Text = jsonString;

                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        dt.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        private void GetTopCountry()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"Select Top 5 Country,Count(*) as Count from Registrations   group by Country order by Count desc";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvCountry.DataSource = dSet.Tables["Table"].DefaultView;
                        gvCountry.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        private void GetLast5Days()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"SELECT top 5  convert(date, DateTime) as 'Date', count(*) as 'Count' FROM Registrations  group by convert(date, DateTime)   order by convert(date, DateTime) desc";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvDays.DataSource = dSet.Tables["Table"].DefaultView;
                        gvDays.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }
        private void GetTopNationality()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"Select Top 5 Nationality,Count(*) as Count from Registrations  group by Nationality order by Count desc";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvNationality.DataSource = dSet.Tables["Table"].DefaultView;
                        gvNationality.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        private void GetRegistrationsCount()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;

                    DateTime dtNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                    DateTime dtCurrent = TimeZoneInfo.ConvertTime(dtNow, TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time"), TimeZoneInfo.Local);// DateTime.Now;

                    DateTime dtToday = new DateTime(dtCurrent.Year, dtCurrent.Month, dtCurrent.Day, dtCurrent.Hour, dtCurrent.Minute, dtCurrent.Second);
                    DateTime dtYesterday = dtToday.AddDays(-1);
                    DateTime dtWeek = dtToday.AddDays(-7);
                    DateTime dtMonth = dtToday.AddDays(-30);

                    string sql = @"SELECT COUNT(*) AS RegAll, 
                            (SELECT COUNT(*) FROM Registrations WHERE DateTime > @Today) AS RegToday,
                            (SELECT COUNT(*) FROM Registrations WHERE DateTime > @YesterdayFrom AND DateTime < @YesterdayTo) AS RegYesterday,
                            (SELECT COUNT(*) FROM Registrations WHERE DateTime > @Week) AS RegWeek,
                            (SELECT COUNT(*) FROM Registrations WHERE DateTime > @Month) AS RegMonth
                           FROM Registrations";

                    cmd.CommandText = sql;

                    cmd.Parameters.AddWithValue("@Today", dtToday);
                    cmd.Parameters.AddWithValue("@YesterdayFrom", dtYesterday);
                    cmd.Parameters.AddWithValue("@YesterdayTo", dtCurrent);
                    cmd.Parameters.AddWithValue("@Week", dtWeek);
                    cmd.Parameters.AddWithValue("@Month", dtMonth);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        TodayCounts.InnerText = reader.GetInt32(reader.GetOrdinal("RegToday")).ToString();
                        YestCounts.InnerText = reader.GetInt32(reader.GetOrdinal("RegYesterday")).ToString();
                        WeekCount.InnerText = reader.GetInt32(reader.GetOrdinal("RegWeek")).ToString();
                        MonthCount.InnerText = reader.GetInt32(reader.GetOrdinal("RegMonth")).ToString();
                    }

                    reader.Close();
                }
            }
        }

        protected void btnGenerateCSV_Click(object sender, EventArgs e)
        {
            generateCSV();
        }

        private void generateCSV()
        {
            using (SqlDataAdapter da = new SqlDataAdapter("select Country,Count(*) As Count from Registrations group by Country", System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                Response.Clear();
                CsvExport csv = new CsvExport();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    csv.AddRow();
                    csv["Country"] = row["Country"];
                    csv["Count"] = row["Count"];
                }

                string sCsv = csv.Export();
                ///   myExport.ExportToFile("Somefile.csv");
                ///   byte[] myCsvData = myExport.ExportToBytes();

                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition", "attachment; filename=CountryCount.csv");

                Response.Write(sCsv);
                Response.End();

            }
        }

        protected void btnDailyReportCSV_Click(object sender, EventArgs e)
        {
            DailyReportCSV();
        }

        private void DailyReportCSV()
        {
            using (SqlDataAdapter da = new SqlDataAdapter("SELECT  convert(date, DateTime) as 'Date', count(*) as 'Total Count' FROM Registrations  group by convert(date, DateTime)   order by convert(date, DateTime) desc", System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                Response.Clear();
                CsvExport csv = new CsvExport();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    csv.AddRow();
                    csv["Date"] = row["Date"];
                    csv["Total Count"] = row["Total Count"];
                }

                string sCsv = csv.Export();
                ///   myExport.ExportToFile("Somefile.csv");
                ///   byte[] myCsvData = myExport.ExportToBytes();

                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition", "attachment; filename=DailyReport.csv");

                Response.Write(sCsv);
                Response.End();

            }
        }

        protected void btnCategoryCSV_Click(object sender, EventArgs e)
        {
           // CategoryCSV();
        }

        private void CategoryCSV()
        {
            using (SqlDataAdapter da = new SqlDataAdapter("select VisitorOrExhibitor,Count(*) As Count from Registrations group by VisitorOrExhibitor", System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                Response.Clear();
                CsvExport csv = new CsvExport();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    csv.AddRow();
                    csv["RegisterType"] = row["VisitorOrExhibitor"];
                    csv["Count"] = row["Count"];
                }

                string sCsv = csv.Export();
                ///   myExport.ExportToFile("Somefile.csv");
                ///   byte[] myCsvData = myExport.ExportToBytes();

                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition", "attachment; filename=CategoryCount.csv");

                Response.Write(sCsv);
                Response.End();

            }
        }

        private void GetCategory()
        {
            var random = new Random();
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"Select VisitorOrExhibitor,Count(ID) as  Count from Registrations  group by VisitorOrExhibitor order by Count desc";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");

                        foreach (DataRow row in dSet.Tables[0].Rows)
                        {
                            var color = String.Format("#{0:X6}", random.Next(0x1000000));
                            PieChartCategory.PieChartValues.Add(new AjaxControlToolkit.PieChartValue
                            {

                                Category = row["VisitorOrExhibitor"].ToString(),
                                Data = Convert.ToDecimal(row["Count"]),
                                PieChartValueColor = color,
                                PieChartValueStrokeColor = color
                            });
                        }

                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        private void GetIntlvslocal()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"SELECT COUNT(*) AS RegAll , 
                            (SELECT COUNT(*) FROM Registrations WHERE Nationality = 'United Arab Emirates' ) AS LocalRegistrationCounts, 
                            (SELECT COUNT(*) FROM Registrations WHERE Nationality != 'United Arab Emirates') AS InternationalRegistrationCounts
                           FROM Registrations ";
                        cmd.CommandText = sql;
                        SqlDataReader reader = cmd.ExecuteReader();


                        while (reader.Read())
                        {
                            totalLocalRegistrationCounts.InnerText = reader.GetInt32(reader.GetOrdinal("LocalRegistrationCounts")).ToString();
                            totalInternationalRegistrationCounts.InnerText = reader.GetInt32(reader.GetOrdinal("InternationalRegistrationCounts")).ToString();

                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        protected void btnNationalityCSV_Click(object sender, EventArgs e)
        {
            NationalityCSV();
        }

        private void NationalityCSV()
        {
            using (SqlDataAdapter da = new SqlDataAdapter("select Nationality,Count(*) As Count from Registrations  group by Nationality", System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                Response.Clear();
                CsvExport csv = new CsvExport();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    csv.AddRow();
                    csv["Country"] = row["Nationality"];
                    csv["Count"] = row["Count"];
                }

                string sCsv = csv.Export();
                ///   myExport.ExportToFile("Somefile.csv");
                ///   byte[] myCsvData = myExport.ExportToBytes();

                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition", "attachment; filename=NationalityCount.csv");

                Response.Write(sCsv);
                Response.End();

            }
        }

        protected void btnGCCCountryCSV_Click_Click(object sender, EventArgs e)
        {
            using (SqlDataAdapter da = new SqlDataAdapter("SELECT COUNT(*) AS Count, Registrations.Country FROM Registrations INNER JOIN  Country ON Registrations.Country = Country.Country  GROUP BY Registrations.Country, Country.Region ", System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                DataSet ds = new DataSet();
                da.Fill(ds);
                Response.Clear();
                CsvExport csv = new CsvExport();
                foreach (DataRow row in ds.Tables[0].Rows)
                {

                    csv.AddRow();
                    csv["Country"] = row["Country"];
                    csv["Count"] = row["Count"];
                }

                string sCsv = csv.Export();
                ///   myExport.ExportToFile("Somefile.csv");
                ///   byte[] myCsvData = myExport.ExportToBytes();

                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition", "attachment; filename=GCCCountryCount.csv");

                Response.Write(sCsv);
                Response.End();

            }
        }

        private void GetGCCCountry()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"SELECT     COUNT(*) AS Count, Registrations.Country
                                    FROM Registrations INNER JOIN  Country ON Registrations.Country = Country.Country
                                    GROUP BY Registrations.Country, Country.Region";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvGCCCountry.DataSource = dSet.Tables["Table"].DefaultView;
                        gvGCCCountry.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        protected void btnDailyListReportCSV_Click(object sender, EventArgs e)
        {
            DailyReportCSV();
        }

        private void GetgvCategory()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"Select VisitorOrExhibitor as RegistrationType,Count(ID) as  Count from Registrations  group by VisitorOrExhibitor order by Count desc";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvCategory.DataSource = dSet.Tables["Table"].DefaultView;
                        gvCategory.DataBind();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }
    }
}