﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdministrationPanel
{
    public class FilterCountry
    {
        public int Count { get; set; }
        public string Country { get;set; }
    }
}