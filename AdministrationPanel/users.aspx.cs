﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class Users : System.Web.UI.Page
    {
        protected MemberInfo member = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.member = new MemberInfo(HttpContext.Current.User.Identity.Name);
            string UserType = this.member.Type;
            if (UserType != "SuperAdmin")
            {
                Response.Write("Access Denied");
                Response.End();
            }
            if (!IsPostBack)
            {
                getUsers();
            }
        }

        private void getUsers()
        {
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    SqlDataAdapter da = null;
                    DataSet dSet = new DataSet();
                    try
                    {
                        string sql = @"Select * from Administrators";
                        da = new SqlDataAdapter(sql, conn);
                        da.Fill(dSet, "Table");
                        gvUsers.DataSource = dSet.Tables["Table"].DefaultView;
                        gvUsers.DataBind();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        dSet.Dispose();
                        da.Dispose();
                        conn.Close();
                        conn.Dispose();
                    }
                }
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            insertUser();
            Response.Redirect(HttpContext.Current.Request.Url.AbsoluteUri);
        }

        private void insertUser()
        {
            string sName = txtName.Text;
            string sUsername = txtUsername.Text;
            string sPassword = txtPassword.Text;
            string sType = ddlAccess.SelectedValue;

            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandTimeout = 500;
                    cmd.CommandText = @"INSERT INTO Administrators(Username,Password,Name,Type) Values(@Username,@Password,@Name,@Type)";
                    cmd.Parameters.Add(new SqlParameter("@Name", sName.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Username", sUsername.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Password", sPassword.Trim()));
                    cmd.Parameters.Add(new SqlParameter("@Type", sType));

                    cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }

            }
        }
    }
}