﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdministrationPanel
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            MemberInfo member = null;
            string sMessage = "";
            using (DataTable dt = LookupUser(txtUsername.Text))
            {
                if (dt.Rows.Count == 0)
                {
                    txtUsername.Focus();
                    sMessage += "<li>Username not availble, Please register.</li>";
                    litMessage.Text = "<ul class=\"form-error\">" + sMessage + "</ul>";
                    txtUsername.Focus();
                    return;
                }

                else
                {
                    int id = Convert.ToInt32(dt.Rows[0]["ID"]);
                    string username = txtUsername.Text;
                    string dbPassword = Convert.ToString(dt.Rows[0]["Password"]);
                    string appPassword = txtPassword.Text;
                    if (string.Compare(dbPassword, appPassword) == 0)
                    {
                        FormsAuthentication.SetAuthCookie(txtUsername.Text, true);
                        FormsAuthentication.RedirectFromLoginPage(txtUsername.Text, true);

                        member = new MemberInfo(id, username);
                        int AdminId = member.ID;
                        insertAdminLogs(AdminId);
                    }
                    else
                    {
                        //You may want to use the same error message so they can't tell which field they got wrong
                        txtPassword.Focus();
                        sMessage += "<li>Invalid Password.</li>";
                        litMessage.Text = "<ul class=\"form-error\">" + sMessage + "</ul>";
                        txtPassword.Focus();
                        return;
                    }
                }

            }
            
        }

        private void insertAdminLogs(int AdminID)
        {
            string ipAddress = "";
            if (Dns.GetHostAddresses(Dns.GetHostName()).Length > 0)
            {
                 ipAddress = Dns.GetHostAddresses(Dns.GetHostName())[0].ToString();
            }
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandTimeout = 500;
                    cmd.Connection = conn;
                    cmd.CommandText = "Insert_Admin_Logs";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter("@AdminID", AdminID));
                    cmd.Parameters.Add(new SqlParameter("@Username", txtUsername.Text));
                    cmd.Parameters.Add(new SqlParameter("@IP", ipAddress));
                    cmd.Parameters.Add(new SqlParameter("@LastLogin", DateTime.Now));

                    cmd.ExecuteNonQuery();
                }
            }
        }
        private static DataTable LookupUser(string Username)
        {
            const string query = "Select ID,Password From Administrators Where Username = @UserName";
            DataTable result = new DataTable();
            using (SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["conString"].ToString()))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = Username;
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        result.Load(dr);
                    }
                }
            }
            return result;
        }
    }
}